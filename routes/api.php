<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

Route::middleware(['api'])->group(function () {
    Route::post('forgot/pwd', 'Auth\ForgotPasswordController@apiForgotEmail');
    Route::post('user', 'UsersController@apiCreateUser');
//    Route::get('playlist/{id}/thumb', 'PlaylistController@apiPlayListThumb')->name("playlist-image");
    Route::post('user/invite/check', 'UsersController@apiCheckInvite');
    Route::middleware(["jwt.auth"])->group(function(){

        Route::get('pet/{id}/vaccines', 'VaccineController@apiListVaccines');
        Route::post('pet/{id}/vaccine/add', 'VaccineController@apiAddVaccine');
        Route::post('pet/{id}/vaccine/delete', 'VaccineController@apiDeleteVaccine');

        Route::get('pet/{id}/notifications', 'PetNotificationsController@apiListNotifications');
        Route::post('pet/{id}/notification/add', 'PetNotificationsController@apiAddNotification');
        Route::post('pet/{id}/notification/delete', 'PetNotificationsController@apiDeleteNotification');

        Route::post('logout', 'AuthController@logout');
        Route::get('search', 'UsersController@apiSearch');
        Route::put('user', 'UsersController@apiEditUser');

        Route::put('user/new-password', 'UsersController@apiEditPwd');
        Route::post('user/follow', 'UsersController@apiFollowUser');
        Route::get('user/{id}/followed', 'UsersController@apiUserFollowed');
        Route::get('pet/{id}/followers', 'PetsController@apiUserFollower');
        Route::get('user/{id}/courses', 'UsersController@apiUserCourses');
        Route::get('user/{id}/groups', 'UsersController@apiUserGroups');
        Route::get('user/{id}/pets', 'UsersController@apiUserPets');
        Route::get('user/{id}/achievements', 'UsersController@apiUserAchievements');
        Route::post('user/block', 'UsersController@apiBlockUser');
        Route::get('user/blockeds', 'UsersController@apiBlockedUsers');
        Route::get('user/{id}', 'UsersController@apiDetailsUser');
        Route::get('user/{id}/posts', 'UsersController@apiPosts');

        Route::get('pet/tag', 'PetsController@apiSearchTag');
        Route::post('pet/edit', 'PetsController@apiEditPet');
        Route::post('pet/delete', 'PetsController@apiDeletePet');
        Route::post('pet', 'PetsController@apiCreatePet');
        Route::get('pet/types', 'PetsController@apiTypeList');
        Route::get('pet/type/{id}/breeds', 'PetsController@apiBreedList');
        Route::get('pet/{id}/posts', 'PetsController@apiPosts');
        Route::get('pet/{id}', 'PetsController@apiDetails');

        Route::get('timeline', 'TimelineController@apiTimeline');
        Route::post('timeline/report', 'TimelineController@apiReport');
        Route::post('user/report', 'UsersController@apiReport');
        Route::post('timeline/store', 'TimelineController@apiStore');
        Route::get('timeline/{id}', 'TimelineController@apiTimelineDetail');
        Route::get('timeline/{id}/comments', 'TimelineController@apiTimelineComments');
        Route::put('timeline/{id}', 'TimelineController@apiUpdate');
        Route::post('timeline/delete', 'TimelineController@apiDestroy');
        Route::post('timeline/like', 'TimelineController@apiUserlike');
        Route::post('timeline/edit', 'TimelineController@apiUpdate');

        Route::post('comment', 'CommentsController@apiAddComment');
        Route::post('comment/edit', 'CommentsController@apiUpdate');
        Route::post('comment/delete', 'CommentsController@apiDestroy');
        Route::post('comment/like', 'CommentsController@apiUserLike');

        Route::post('group', 'GroupsController@apiStore');
        Route::put('group/{id}', 'GroupsController@apiUpdate');
        Route::delete('group/{id}', 'GroupsController@apiDestroy');
        Route::get('group/{id}/member', 'GroupsController@apiMembers');
        Route::get('group/{id}/details', 'GroupsController@apiDetails');
        Route::post('group/{id}/member', 'GroupsController@apiSaveMember');
        Route::delete('group/{id}/member', 'GroupsController@apiLeaveMember');

        Route::get('notifications', 'UsersController@apiNotifications');

        Route::post('course', 'CoursesController@apiStore');
        Route::get('course/payments', 'CoursesController@apiPayments');
        Route::get('course/{id}', 'CoursesController@apiDetails');
        Route::put('course/{id}', 'CoursesController@apiUpdate');
        Route::delete('course/{id}', 'CoursesController@apiDestroy');
        Route::post('course/{id}/signature', 'CoursesController@apiSignature');
        Route::post('course/{id}/rate', 'CoursesController@apiRate');
//        Route::post('course/{courseId}/meditation', 'MeditationsController@apiAddCourseMeditation');

        Route::group(["prefix" => "chat"], function() {
            Route::get("stickers", "ChatController@apiListStickers");
            Route::get("rooms", "ChatController@apiListRooms");
            Route::get("room", "ChatController@apiRoom");
            Route::post("room", "ChatController@apiPostMessage");
        });

        Route::group(["prefix" => "tricks"], function() {
            Route::get("groups", "TricksController@apiListGroups");
            Route::get("group/{id}", "TricksController@apiGroupTricks");
            Route::get("trick/{id}", "TricksController@apiTrickDetail");

            Route::post("set-state", "TricksController@apiTrickUpateState");

            Route::get('{id}/notifications', 'PetNotificationsController@apiListTrickNotifications');
            Route::post('{id}/notification/add', 'PetNotificationsController@apiAddTrickNotification');
            Route::post('{id}/notification/delete', 'PetNotificationsController@apiTrickDeleteNotification');
        });

    });
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


