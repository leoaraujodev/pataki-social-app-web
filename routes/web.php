<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/info-1230', function () {
    phpinfo();
    exit;
});

Route::get('/dashboard', function () {
    echo "<h1>entrou</h1>";
});

// Password reset link request routes...
Route::get('password/email', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.email');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');

// Password reset routes...
Route::get('password/reset', 'Auth\ResetPasswordController@showResetForm')->name('password.request');
Route::post('password/reset', 'Auth\ResetPasswordController@postReset')->name('password.reset');
Route::get('password-reset', 'Auth\ResetPasswordController@viewPasswordReset')->name('password.reset');

Auth::routes();

Route::middleware(["auth"])->group(function()
{
    Route::get('/home', 'HomeController@home')->name('home');
    Route::get('/admin/users', 'UsersController@index')->name('user.index');
    Route::get('/admin/courses', 'CoursesController@index')->name('courses.index');
//    Route::get('/admin/meditations', 'MeditationsController@index')->name('courses.index');
    Route::get('/admin/user/{uuid}/details', 'UsersController@userDetails')->name('user-details');
    Route::get('/user/{uuid}/set-as-admin', 'UsersController@userSetAsAdmin')->name('user.set-as-admin');
    Route::get('/user/{uuid}/remove-from-admin', 'UsersController@userRemoveFromAdmin')->name('user.remove-from-admin');

    Route::resource('user', 'UsersController');
    Route::get("/admin/tricks","TricksController@adminGroupTricks")->name('trick.index');
    Route::get('/admin/tricks/{id}', 'TricksController@adminGroupTricksDetail')->name('trick.group-detail');
    Route::get('/admin/tricks/{groupId}/trick/{id}', 'TricksController@adminTricksDetail')->name('trick.detail');
    Route::put('/admin/trick/{id}', 'TricksController@adminTricksEdit')->name('trick.update');

    Route::post("/admin/tricks","TricksController@adminTricksSave")->name('trick.trick-action');


    Route::post("/admin/tricks/step","TricksController@submitFormStep")->name('trick.step-action');
    Route::post("/admin/tricks/step/delete","TricksController@deleteFormStep")->name('trick.step-delete');

    Route::post("/admin/tricks/tip","TricksController@submitFormTip")->name('trick.tip-action');
    Route::post("/admin/tricks/tip/delete","TricksController@deleteFormTip")->name('trick.tip-delete');
});


//Route::get('/home', function() {
//
//})->name('home')->middleware('auth');
