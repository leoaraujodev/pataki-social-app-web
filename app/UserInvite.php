<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInvite extends Model
{
    protected $fillable = ['inviter_id','user_id','email','name'];
    protected $hidden = ["id","user_id","inviter_id","updated_at"];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
