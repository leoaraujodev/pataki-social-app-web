<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ["tag"];
    protected $hidden   = ["created_at","updated_at","pivot"];

    public static function stringToTags($tags)
    {
        if (!is_array($tags)) {
            $arrTag[] = $tags;
            $tags = $arrTag;
        }

        $outPut = [];
        foreach ($tags as $tag) {
            if ($tag[0] == "#") {
                $tag = substr($tag, 1);
            }

            $objTag = Tag::where("tag","ilike",$tag)->first();
            if ($objTag) {
                $outPut[] = $objTag;
            } else {
                $outPut[] = Tag::create(["tag"=>$tag]);
            }
        }
        return $outPut;
    }
}
