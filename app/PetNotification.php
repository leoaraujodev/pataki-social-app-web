<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PetNotification extends Model
{
    protected $fillable = [
        'name', 'pet_id', 'time','on_sun', 'on_mon', 'on_tue', 'on_wed', 'on_thu', 'on_fri', 'on_sat','trick_id','user_id'
    ];

    protected $hidden = ["pet_id","created_at","updated_at",'trick_id'];


    public function trick()
    {
        return $this->belongsTo(Tricks::class, 'trick_id');
    }

    public function pet()
    {
        return $this->belongsTo(Pet::class, 'pet_id');
    }
}
