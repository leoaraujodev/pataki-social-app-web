<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;

class TrickSteps extends Model
{

    const IMG_PATH = "tricks/steps";
    protected $fillable = ["desc","order","file","trick_id"];
    protected $hidden = ["order","trick_id"];
    protected $appends  = ["image_path"];
    public $timestamps = false;

    public function getImagePathAttribute(){
        if(empty($this->file)) {
            return;
        }
        return Storage::url($this->file);
    }
}
