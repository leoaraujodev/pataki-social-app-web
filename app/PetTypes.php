<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PetTypes extends Model
{
    protected $fillable = ["name"];
    protected $hidden = ["created_at","updated_at"];

    public function defaultImg()
    {
        return asset("img/pet_types/default_". $this->id .".png");
    }
}
