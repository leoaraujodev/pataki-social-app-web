<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pet;

class Vaccine extends Model
{
    protected $fillable = [
        'name', 'pet_id', 'date'
    ];

    protected $hidden = ["pet_id","created_at","updated_at"];

    protected $dates = ["date"];

    public function pet()
    {
        return $this->belongsTo(Pet::class, 'pet_id');
    }
}
