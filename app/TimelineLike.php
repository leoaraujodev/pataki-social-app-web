<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimelineLike extends Model
{
    protected $fillable = ["timeline_id", "user_id"];
}
