<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGroups extends Model
{
    const kADMIN = 1;
    const kUSER = 0;

    protected $fillable = ['group_id', 'user_id',"profile"];
    protected $hidden   = ["created_at", "updated_at", "group_id", "user_id", "id"];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
