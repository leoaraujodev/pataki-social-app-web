<?php

namespace App\Http\Controllers;

use App\OneSignalNotification;
use App\Timeline;
use App\UserNotification;
use Illuminate\Http\Request;
use Auth;
use App\Comment;
use App\CommentLike;

class CommentsController extends Controller
{
    public function apiAddComment(Request $request)
    {
        $user   = Auth::user();
        $data   = $request->all();

        $post = Timeline::findOrFail($data["timeline_id"]);
        if($post->pet->tutor->is_blocked) {
            return response()->json(null, 404);
        }

        $data["user_id"] = $user->id;

        $comment = Comment::create($data);

        if ($user->id != $comment->timeline->user_id) {
            UserNotification::create([  'action'        => 'post_comment',
                                        'notified_id'   => $comment->timeline->user_id,
                                        'user_id'       => $user->id,
                                        'timeline_id'   => $comment->timeline->id,
                                        "message"       => "comentou em sua postagem."]);



            if (!is_null($comment->timeline->user->onesiginal_token)) {
                OneSignalNotification::sendPushNotification($user->name. " comentou em sua postagem.",
                    $comment->timeline->user->onesiginal_token);
            }
        }

        foreach ($comment->mention_pets as $mentionPet)
        {
            if ($mentionPet->tutor->onesiginal_token) {
                OneSignalNotification::sendPushNotification("O pet ". $mentionPet->name ." foi mencionado em um comentário.",
                    $mentionPet->tutor->onesiginal_token);
            }

            UserNotification::create([  'action'        => 'post_comment',
                                        'notified_id'   => $mentionPet->tutor->id,
                                        'user_id'       => $user->id,
                                        'pet_id'        => $mentionPet->id,
                                        'timeline_id'   => $comment->timeline->id,
                                        "message"       => "mencionou o pet ". $mentionPet->name ." em um comentário."]);
        }

        return response()->json($comment, $this->requestCreatedSuccess);
    }

    public function apiUpdate(Request $request)
    {
        $id     = $request->input("id");
        $user   = Auth::user();
        $data   = $request->all();

        $comment = Comment::findOrFail($id);
        if ($comment->user_id != $user->id) {
            return response()->json(["error" => "Requisição inválida"], 400);
        }
        $comment->update($data);

        return response()->json(['comment' => $comment]);
    }

    public function apiDestroy(Request $request)
    {
        $id     = $request->input("id");
        $user = Auth::user();

        $comment = Comment::find($id);

        if ($comment->user_id != $user->id) {
            return response()->json(["error" => "Requisição inválida"], 400);
        }

        $comment->delete();
        return response()->json(["message" => "Removido com sucesso"], $this->requestAcceptedSuccess);
    }

    public function apiUserLike(Request $request)
    {
        $user   = Auth::user();
        $commentId = $request->input("comment_id");

        $status = $request->input("like", false);

        $comment = Comment::findOrFail($commentId);
        $bless = CommentLike::whereUserId($user->id)->whereCommentId($commentId)->first();
        if ($status) {
            if (!$bless) {
                CommentLike::create([  "user_id"       => $user->id,
                                        "comment_id"   => $commentId]);
                $comment->tot_likes += 1;
                $comment->update();
            }
            return response()->json(["message" => "Registrado com sucesso."], $this->requestAcceptedSuccess);
        } else {
            if ($bless) {
                $bless->delete();
                $comment->tot_likes -= 1;
                $comment->update();
            }
            return response()->json(["message" => "Removido com sucesso."], $this->requestAcceptedSuccess);
        }
    }
}
