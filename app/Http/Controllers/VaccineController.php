<?php

namespace App\Http\Controllers;

use App\Pet;
use App\Vaccine;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VaccineController extends Controller
{
    public function apiListVaccines($id)
    {
        $pet = Pet::findOrFail($id);
        if (Auth::user()->id != $pet->user_id) {
            return response()->json(['message' => __("Não foi possível realizar a ação.")], $this->requestNotAcceptable);
        }
        return $pet->vaccines;
    }

    public function apiAddVaccine($id, Request $request)
    {
        $data = $request->all();
        $data["pet_id"] = $id;

        $vaccine = Vaccine::create($data);
        return $vaccine;
    }

    public function apiDeleteVaccine($id, Request $request)
    {

        $vaccine = Vaccine::findOrFail($request->input("id",0));
        if (Auth::user()->id != $vaccine->pet->user_id) {
            return response()->json(['message' => __("Não foi possível realizar a ação.")], $this->requestNotAcceptable);
        }

        $vaccine->delete();
        return response()->json(['message' => __("Removido com sucesso.")]);
    }

}
