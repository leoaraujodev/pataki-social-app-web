<?php

namespace App\Http\Controllers;

use App\PetNotification;
use Illuminate\Http\Request;
use App\Pet;
use Illuminate\Support\Facades\Auth;

class PetNotificationsController extends Controller
{
    public function apiListNotifications($id)
    {
        $pet = Pet::findOrFail($id);
        if (Auth::user()->id != $pet->user_id) {
            return response()->json(['message' => __("Não foi possível realizar a ação.")], $this->requestNotAcceptable);
        }
        return $pet->notifications;
    }



    public function apiAddTrickNotification($id, Request $request)
    {
        $data = $request->all();
        $data["trick_id"] = $id;
        $data["user_id"] = Auth::user()->id;

        $time = $data["time"];

        if (strlen($time) != 5 || $time[2] != ":") {
            return response()->json(['message' => __("Campo hora inválido.")], $this->requestNotAcceptable);
        }

        $notification = PetNotification::create($data);
        return $notification;
    }

    public function apiAddNotification($id, Request $request)
    {
        $data = $request->all();
        $data["pet_id"] = $id;

        $time = $data["time"];

        if (strlen($time) != 5 || $time[2] != ":") {
            return response()->json(['message' => __("Campo hora inválido.")], $this->requestNotAcceptable);
        }

        $notification = PetNotification::create($data);
        return $notification;
    }

    public function apiTrickDeleteNotification($id, Request $request)
    {

        $notification = PetNotification::findOrFail($request->input("id",0));
        if (Auth::user()->id != $notification->user_id) {
            return response()->json(['message' => __("Não foi possível realizar a ação.")], $this->requestNotAcceptable);
        }

        $notification->delete();
        return response()->json(['message' => __("Removido com sucesso.")]);
    }

    public function apiDeleteNotification($id, Request $request)
    {

        $notification = PetNotification::findOrFail($request->input("id",0));
        if (Auth::user()->id != $notification->pet->user_id) {
            return response()->json(['message' => __("Não foi possível realizar a ação.")], $this->requestNotAcceptable);
        }

        $notification->delete();
        return response()->json(['message' => __("Removido com sucesso.")]);
    }
}
