<?php

namespace App\Http\Controllers;

use App\BlockedUser;
use App\OneSignalNotification;
use App\Pet;
use App\TimelineReport;
use App\UserNotification;
use Illuminate\Http\Request;
use App\Timeline;
use Auth;
use Illuminate\Support\Facades\Mail;
use Storage;
use File;
use App\TimelineLike;
use App\Comment;
use App\User;
use App\UserFollow;
use Intervention\Image\Facades\Image;

class TimelineController extends Controller
{
    public function apiTimeline(Request $request)
    {
        $user = Auth::user();

        $usersBlockedMe = BlockedUser:: whereBlockedId(Auth::user()->id)->
                                        pluck("user_id")->
                                        toArray();

        $petsOrigem     = UserFollow::  where("user_id", "=", $user->id)->
                                        pluck("pet_id")->
                                        toArray();
        $myPets     =   Pet::   whereUserId($user->id)->
                                pluck("id")->
                                toArray();

        $petsOrigem = array_merge($petsOrigem, $myPets);
        $petsOrigem[] = 10;

        $reportedPosts = TimelineReport::   where("user_id", "=", $user->id)->
                                            pluck("timeline_id")->
                                            toArray();

        $timeline = Timeline::  with(["user",'pet'])->
                                whereIn("pet_id", $petsOrigem)->
                                whereNotIn("id", $reportedPosts)->
                                whereNotIn("user_id", $usersBlockedMe)->
                                whereNull("group_id")->
                                orderBy("id","desc")->
                                limit(15);

        $lastId = $request->input("last_id",0);
        if ($lastId > 0) {
            $timeline = $timeline->where("id","<", $lastId);

        }

        $output = $timeline->get();
        return ["data"=> $output];
    }

    public function apiStore(Request $request)
    {
        $user   = Auth::user();
        $data   = $request->all();
        $image  = $request->file("image");

        $data["user_id"] = $user->id;

        if ((!isset($data["desc"]) || strlen($data["desc"]) == 0) && !$request->hasFile("image")) {
            return response()->json(['message' => "Envie imagem ou texto"], $this->requestNotAcceptable);
        }

        if ((!isset($data["pet_id"]) || strlen($data["pet_id"]) == 0)) {
            return response()->json(['message' => "Informe o pet."], $this->requestNotAcceptable);
        }

        $hasPetPermission = Pet::whereId($data["pet_id"])->whereUserId($user->id)->exists();

        if (!$hasPetPermission) {
            return response()->json(['message' => "Informe o pet."], $this->requestNotAcceptable);
        }

        $timeline = Timeline::create($data);
        $timeline = Timeline::find($timeline->id);
        $timeline->user;
        $timeline->pet;
        if($request->hasFile("image") &&  $image->isValid()) {
            $this->uploadFile($timeline, $image);
        }

        foreach ($timeline->mention_pets as $mentionPet)
        {
            if ($mentionPet->tutor->onesiginal_token) {
                OneSignalNotification::sendPushNotification("O pet ". $mentionPet->name ." foi mencionado em uma postagem.",
                    $mentionPet->tutor->onesiginal_token);
            }

            UserNotification::create([  'action'        => 'post_comment',
                                        'notified_id'   => $mentionPet->tutor->id,
                                        'user_id'       => $user->id,
                                        'pet_id'        => $mentionPet->id,
                                        'timeline_id'   => $timeline->id,
                                        "message"       => "mencionou o pet ". $mentionPet->name ."."]);
        }

        return response()->json($timeline, $this->requestCreatedSuccess);
    }

    public function apiUpdate(Request $request)
    {
        $id     = $request->input("id");
        $user   = Auth::user();
        $data   = $request->all();
        $image  = $request->file("image");

        $timeline = Timeline::find($id);
        if ($timeline->user_id != $user->id) {
            return response()->json(["error" => "Requisição inválida"], 400);
        }

        if ((!isset($data["desc"]) || strlen($data["desc"]) == 0) && !$request->hasFile("image")) {
            return response()->json(['message' => "Envie imagem ou texto"], $this->requestNotAcceptable);
        }

        if($request->input("remove_image", 0)  == 1) {
            Storage::delete($timeline->filename);
            $timeline->filename = null;
            $timeline->update();
        }

        $timeline->update($data);

        if($request->hasFile("image") &&  $image->isValid()) {
            $this->uploadFile($timeline, $image);
        }

        return response()->json($timeline);
    }

    public function apiDestroy(Request $request)
    {
        $id   = $request->input("id");
        $user = Auth::user();

        $timeline = Timeline::find($id);

        if ($timeline->user_id != $user->id) {
            return response()->json(["error" => "Requisição inválida"], 400);
        }

        $timeline->delete();
        return response()->json(["message" => "Removido com sucesso"], $this->requestAcceptedSuccess);
    }

    public function apiReport(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $timeline = Timeline::findOrFail($request->input("id"));

        if (!TimelineReport::where("user_id","=", $user->id)->where("timeline_id","=", $timeline->id)->exists()) {
            $report = TimelineReport::create([  "user_id"       => $user->id,
                                                "timeline_id"   => $timeline->id,
                                                "reason"        => $request->input("reason",""),
                                                "comment"       => $request->input("comment","")]);
            if ($report) {

//                $sendEmail = Mail:: to("jsinoti@gmail.com")->
//                            send(new \App\Mail\TimelineReport());

                return response()->json(["message" => "Postagem reportada com sucesso, em até 24 horas iremos revisar a postagem."], $this->requestAcceptedSuccess);
            }

        }
        return response()->json(["error" => "Requisição inválida"], 400);
    }

    public function apiTimelineDetail($id)
    {
        $timeline = Timeline::whereId($id)->with(["user","pet"])->first();

        if($timeline->pet->tutor->is_blocked) {
            return response()->json(null, 404);
        }

        $timeline = $timeline->toArray();

        $users      = User::    join('timeline_likes', 'users.id', '=', 'timeline_likes.user_id')->
                                where('timeline_likes.timeline_id', '=', $id)->
                                get();

        $comments   = Comment:: wheretimelineId($id)->
                                with(['user'])->
                                limit(5)->
                                orderBy("id","desc")->
                                get();

        $timeline["likes"] = $users;
        $timeline["comments"] = $comments;
        return $timeline;
    }

    public function apiUserlike(Request $request)
    {
        $user   = Auth::user();
        $timelineId = $request->input("timeline_id");

        $status = $request->input("like", false);

        $timeline = Timeline::findOrFail($timelineId);
        if($timeline->pet->tutor->is_blocked) {
            return response()->json(null, 404);
        }

        $liked = TimelineLike::whereUserId($user->id)->whereTimelineId($timelineId)->first();
        if ($status) {
            if (!$liked) {
                TimelineLike::create([ "user_id"       => $user->id,
                                        "timeline_id"   => $timelineId]);
                $timeline->tot_likes += 1;
                $timeline->update();

                if ($user->id != $timeline->user_id) {
                    UserNotification::create([  'action'        => 'post_like',
                                                'notified_id'   => $timeline->user_id,
                                                'user_id'       => $user->id,
                                                'timeline_id'   => $timeline->id,
                                                "message"       => "curtiu sua postagem."]);

                    if (!is_null($timeline->user->onesiginal_token)) {
                        OneSignalNotification::sendPushNotification($user->name. " curtiu sua postagem.",
                            $timeline->user->onesiginal_token);
                    }
                }

            }
            return response()->json(["message" => "Registrado com sucesso."], $this->requestAcceptedSuccess);
        } else {
            if ($liked) {
                $liked->delete();
                $timeline->tot_likes -= 1;
                $timeline->update();
            }
            return response()->json(["message" => "Removido com sucesso."], $this->requestAcceptedSuccess);
        }
    }

    public function apiTimelineComments(Request $request, $id)
    {
        $comments = Comment::   where("timeline_id", "=", $id)->
                                whereNotIn("user_id", Auth::user()->blockedUserIds())->
                                with("user")->
                                orderBy("id","desc");

        $comments = $comments->paginate()->toArray();

        unset($comments["first_page_url"]);
        unset($comments["last_page_url"]);
        unset($comments["next_page_url"]);
        unset($comments["prev_page_url"]);
        unset($comments["path"]);
        return $comments;
    }

    public function uploadFile($timeline, $image)
    {
        if(!empty($timeline->filename)) {
            Storage::delete($timeline->filename);
        }

        if(substr($image->getMimeType(), 0, 5) == "image") {
            $image_thumb = Image::make($image);
            if($image_thumb->width() > $image_thumb->height()) {
                $image_thumb->heighten(800, function ($constraint) {
                    $constraint->aspectRatio();
                });
            } else {
                $image_thumb->widen(800, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }

            $image_thumb = $image_thumb->resizeCanvas(800,800);
            $image_thumb = $image_thumb->stream();

            $fileName = $timeline->id.time() .".". File::extension($image->getClientOriginalName());
            $fileName = Timeline::IMG_PATH."/".$fileName;

            Storage::disk('public')->put($fileName, $image_thumb->__toString(), "public");
            $timeline->filename = $fileName;
        } else {
            $fileName = $timeline->id.time().".".File::extension($image->getClientOriginalName());
            Storage::putFileAs(Timeline::IMG_PATH, $image, $fileName,'public');
            $timeline->filename = Timeline::IMG_PATH."/".$fileName;
        }

        $timeline->update();
    }
}
