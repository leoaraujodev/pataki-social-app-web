<?php

namespace App\Http\Controllers;

use App\BlockedUser;
use App\Firestore;
use App\Group;
use App\Mail\SendInvite;
use App\OneSignalNotification;
use App\Pet;
use App\PetBreed;
use App\Tag;
use App\Timeline;
use App\TimelineReport;
use App\UserFollow;
use App\UserInvite;
use App\UserNotification;
use App\UserReport;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Tymon\JWTAuth\JWT;
use Validator;
use App\User;
use Auth;
use App\Course;
use JWTAuth;
use App\UserGroups;
use App\CourseSignature;
use Illuminate\Support\Facades\Redirect;
use Storage;
use File;
use Mail;
use Webpatser\Uuid\Uuid;

class UsersController extends Controller
{

    public function index()
    {
        $users = User:: orderBy('name')->
                        paginate();
        return view('user.index', compact('users'));
    }

    public function destroy($id) {
        $loggedUser = Auth::user();
        $user = User::findOrFail($id);
        if( $loggedUser->is_admin == true || $loggedUser->id != $user->id) {
            $user->delete();
            return response()->redirectToRoute('user.index', $user->uuid)->with('success', "User removed");
        }
        exit();

    }

    public function apiCreateUser(Request $request)
    {
        $data = $request->all();
        $rules = [
            'name'      => 'required',
            'password'  => 'required|min:8',
            'email'     => 'required|email|unique:users,email',
        ];

        $validator = Validator::make($data,$rules);
        $error = "";
        if ($validator->fails()) {
            $messages = $validator->messages()->all();
            return response()->json(['message' => $messages[0]], $this->requestNotAcceptable);
        } else {

            $data["password"] = bcrypt($data["password"]);
            $data["uuid"] = Uuid::generate(4);

            $user = User::create($data);

            $credentials = request(['email', 'password']);
            $token = JWTAuth::attempt($credentials);

            $dataEmail = ["user_name"=> $user->name];
            $sendEmail = Mail:: to($user->email)->
                                send(new \App\Mail\WelcomeMail($dataEmail));

            return response()->json([   'access_token' => $token,
                                        'user'  => $user,
                                    ]);
        }
    }

    public function apiEditUser(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $rules = [
            'password'  => 'min:8',
            'email'     => 'email|unique:users,email,'.$user->id,
        ];

        $validator = Validator::make($data,$rules);

        if ($validator->fails()) {
            $messages = $validator->messages()->all();
            if (count($messages) > 1) {
                return response()->json(['messages' => $messages], $this->requestNotAcceptable);
            } else {
                return response()->json(['message' => $messages[0]], $this->requestNotAcceptable);
            }
        } else {
            if (isset($data["password"])) {
                $data["password"] = bcrypt($data["password"]);
            }

            $user->update($data);

            $image  = $request->file("image");
            if($request->hasFile("image") &&  $image->isValid()) {
                $this->uploadFile($user, $image);
            }

            return response()->json(['user' => $user]);
        }
    }

    public function apiUserPets($id)
    {

        $pet = Pet::whereUserId($id)->
                    with(["breed","breed.type"])->
                    get();

        $user = User::findOrFail($id);

        if($user->is_blocked) {
            return response()->json(null, 404);
        }

        return $pet;
    }

    public function apiSetDefaultTags(Request $request) {
        $tags = $request->input("tags");
        if (!empty($tags) && count($tags)) {
            $tags = Tag::stringToTags($tags);
            $user = Auth::user();
            $user->addTagRelation($tags);
            $user->tags;

            return response()->json(['user' => $user], $this->requestCreatedSuccess);
        } else {
            return response()->json(['message' => "Select one tag"], $this->requestNotAcceptable);
        }
    }

    public function userDetails($uuid)
    {
        $user = User::where("uuid","=", $uuid)->
                      first();
        if ($user) {
            $userInfo = [];
            return view('user.details', compact('userInfo','user'));
        } else {
            return Redirect::back()->with('error', "User not found");
        }

    }

    public function userSetAsAdmin($uuid)
    {
        $user = User::byUUID($uuid);
        if ($user) {
            $user->is_admin = true;
            $user->update();
            return response()->redirectToRoute('user-details', $user->uuid)->with('success', "User updated");
        } else {
            return Redirect::back()->with('error', "User not found");
        }
    }

    public function userRemoveFromAdmin($uuid)
    {
        $user = User::byUUID($uuid);
        if ($user) {
            $user->is_admin = false;
            $user->update();
            return response()->redirectToRoute('user-details', $user->uuid)->with('success', "User updated");
        } else {
            return Redirect::back()->with('error', "User not found");
        }
    }

    public function apiReport(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $reported = User::findOrFail($request->input("id"));
        $report = UserReport::create([  "user_id"       => $user->id,
                                        "reported_id"   => $reported->id,
                                        "reason"        => $request->input("reason",""),
                                        "comment"       => $request->input("comment","")]);
        if (!UserReport::where("user_id","=", $user->id)->where("reported_id","=", $reported->id)->exists()) {
            if ($report) {
//                $sendEmail = Mail:: to("jsinoti@gmail.com")->
//                            send(new \App\Mail\TimelineReport());
                return response()->json(["message" => "Usuário reportado com sucesso."], $this->requestAcceptedSuccess);
            }
        }
        return response()->json(["error" => "Usuário já reportado anteriormente"], $this->requestNotAcceptable);
    }


    public function apiDetailsUser(Request $request, $id)
    {
        $user = User::find($id);

        if ($user->is_blocked) {
            return response()->json(null, 404);
        }

        if ($user) {
            $user = $user->toArray();
            $user["pets"] = Pet::   with(["breed","breed.type"])->
                                    whereUserId($id)->get();
            $user["following"] = UserFollow::whereUserId($id)->count();
            $user["groups"] = UserGroups::whereUserId($id)->count();
            $user["courses"] = CourseSignature::whereUserId($id)->count();
            return response()->json(['user' => $user]);
        } else {
            return response()->json(['message' => __("User not found")], $this->requestNotAcceptable);
        }
    }

    public function apiFollowUser(Request $request)
    {
        $user = Auth::user();
        $petId = $request->input("pet_id");

        $pet = Pet::findOrFail($petId);
        if($pet->tutor->is_blocked) {
            return response()->json(null, 404);
        }

        $isToFollow = $request->input("follow");
        $userFollow = UserFollow::  where("user_id", "=", $user->id)->
                                    where("pet_id", "=", $petId)->first();

        if ((bool)$isToFollow) {
            if (!$userFollow) {
                $pet = Pet::findOrFail($petId);

                UserFollow::create(["user_id" => $user->id, "pet_id" => $petId]);

                if ($user->id != $pet->user_id) {
                    UserNotification::create([  'action'        => 'follow',
                                                'notified_id'   => $pet->user_id,
                                                'user_id'       => $user->id,
                                                'pet_id'        => $petId,
                                                "message"       => "começou a seguir"]);

                    if (!is_null($pet->tutor->onesiginal_token)) {
                        OneSignalNotification::sendPushNotification($user->name. " começou a seguir ". $pet->name.".",
                            $pet->tutor->onesiginal_token);
                    }
                }

            }
            return response()->json(['message' => __("Solicitação realizada com sucesso")]);
        } else {
            if ($userFollow) {
                $userFollow->delete();
            }
            return response()->json(['message' => __("Solicitação realizada com sucesso")]);
        }
    }

    public function apiUserFollowed(Request $request, $id)
    {

        $user = User::findOrFail($id);
        if($user->is_blocked) {
            return response()->json(null, 404);
        }

        $usersBlockedMe = BlockedUser:: whereBlockedId(Auth::user()->id)->
                                        pluck("user_id")->
                                        toArray();

        $usersIBlocked = BlockedUser::  whereUserId(Auth::user()->id)->
                                        pluck("blocked_id")->
                                        toArray();

        $userFollowed = Pet::   select("pets.*")->
                                with(["tutor"])->
                                join("users","users.id",'=','pets.user_id')->
                                leftJoin('user_follows', 'pets.id', '=', 'pet_id')->
                                whereNotIn("pets.user_id", $usersBlockedMe)->
                                whereNotIn("pets.user_id", $usersIBlocked)->
                                where("user_follows.user_id", "=", $id);

        if($request->has("term")) {
            $term = $request->input("term");
            $userFollowed = $userFollowed->where(function($query) use ($term)
            {
                $query->where('pets.name','ilike','%'.$term.'%')->
                        orWhere('users.name','ilike','%'.$term.'%');
            });
//            dd("a");
        }

        $userFollowed = $userFollowed->get();
        return $userFollowed;
    }

    public function apiUserGroups($id)
    {
        $user = User::findOrFail($id);
        if($user->is_blocked) {
            return response()->json(null, 404);
        }

        $groups = Group::   select("groups.*")->
                            leftJoin('user_groups', 'groups.id', '=', 'user_groups.group_id')->
                            where("user_groups.user_id", "=", $id)->
                            get();

        return $groups;
    }

    public function apiUserCourses($id)
    {
        $data["my"] = Course::whereUserId($id)->get();

        $signedCourses = Course::   select("courses.*")->
                                    with(["user"])->
                                    join("course_signatures","courses.id","=","course_id")->
                                    where("course_signatures.user_id","=", $id)->
                                    get();

        $data["signed"] = $signedCourses;
        return $data;
    }

    public function apiPosts($id, Request $request)
    {
        $user = Auth::user();
        $postUser = User::findOrFail($id);

        if($postUser->is_blocked) {
            return response()->json(null, 404);
        }

        $reportedPosts = TimelineReport::   where("user_id", "=", $user->id)->
                                            pluck("timeline_id")->
                                            toArray();

                                            $timeline = Timeline::  with(["user",'pet'])->
                                                                    whereUserId($postUser->id)->
                                                                    whereNotIn("id", $reportedPosts)->
                                                                    whereNull("group_id")->
                                                                    orderBy("id","desc")->
                                                                    limit(15);

        $lastId = $request->input("last_id",0);
        if ($lastId > 0) {
            $timeline = $timeline->where("id","<", $lastId);
        }

        $output = $timeline->get();
        return ["data"=> $output];
    }

    public function apiEditPwd(Request $request)
    {
        $user = Auth::user();

        $curPwd = $request->input("cur_password");
        $newPwd = $request->input("new_password");
        if (\Hash::check($curPwd, $user->password))
        {
            if(strlen($newPwd) < 6) {
                return response()->json(["error" => "Nova senha deve conter ao menos 6 caracteres."], $this->requestNotAcceptable);
            }

            $user->password = bcrypt($newPwd);
            $user->update();
            return response()->json(["success" => "Senha atualizada com sucesso."]);
        } else {
            return response()->json(["message" => "Senha atual inválida."], $this->requestNotAcceptable);
        }

    }

    public function apiSearch(Request $request)
    {
        $perPage    = 10;
        $term       = $request->input("term","");
        $curPage    = $request->input("page");

        $usersBlockedMe = BlockedUser:: whereBlockedId(Auth::user()->id)->
                                        pluck("user_id")->
                                        toArray();

        $usersIBlocked = BlockedUser::  whereUserId(Auth::user()->id)->
                                        pluck("blocked_id")->
                                        toArray();

        if (strlen($term) < 3) {

            $outPut = [ "current_page"  => 1,
                        "per_page"      => 10,
                        "has_next_page" => false];


            $users = User:: whereNotIn("id", $usersBlockedMe)->
                            whereNotIn("id", $usersIBlocked)->
                            where("id", "!=", 10)->
                            limit(5)->
                            orderBy("id","desc")->
                            get()->
                            toArray();

            $outPut["tutors"] = $users;

            $pets = Pet::   with(["tutor","breed","breed.type"])->
                            whereNotIn("user_id", $usersBlockedMe)->
                            whereNotIn("user_id", $usersIBlocked)->
                            where("id", "!=", 10)->
                            limit(5)->
                            orderBy("id","desc")->
                            get()->
                            toArray();

            $outPut["pets"] = $pets;

            $groups = Group::   with(["user"])->
                                whereNotIn("user_id", $usersBlockedMe)->
                                whereNotIn("user_id", $usersIBlocked)->
                                where("privacy","=", "public")->
                                limit(5)->
                                orderBy("id","desc")->
                                get()->
                                toArray();

            $outPut["groups"] = $groups;


            return $outPut;
        }

        $hasFilter = $request->has("filter");
        $filter = $request->input("filter","-");
        if ($filter != "tutors" &&
            $filter != "groups" &&
            $filter != "pets") {
            $hasFilter =  false;
        }

        if (!$hasFilter || $filter == "tutors")
        {
            $users = User:: where("name","ilike", "%".$term."%")->
                            whereNotIn("id", $usersBlockedMe)->
                            whereNotIn("id", $usersIBlocked)->
                            where("id", "!=", 10)->
                            paginate($perPage+1)->
                            toArray();
        }

        if (!$hasFilter || $filter == "pets") {
            $breeds = PetBreed::where("name","ilike", "%".$term."%")->pluck("id");

            $pets = Pet::   with(["tutor","breed","breed.type"])->
                            whereNotIn("user_id", $usersBlockedMe)->
                            whereNotIn("user_id", $usersIBlocked)->
                            where("id", "!=", 10);

            if (count($breeds)) {
                $pets   = $pets->where(function($query) use ($breeds, $term)
                                {
                                    $query->where("name","ilike", "%".$term."%")->
                                            orWhereIn("breed_id",$breeds);
                                });
            } else {
                $pets = $pets->where("name","ilike", "%".$term."%");
            }

            $pets = $pets->paginate($perPage+1)->
                           toArray();
        }

        if (!$hasFilter || $filter == "groups")
        {
            $groups = Group::   with(["user"])->
                                where("name","ilike", "%".$term."%")->
                                whereNotIn("user_id", $usersBlockedMe)->
                                whereNotIn("user_id", $usersIBlocked)->
                                where("privacy", "=", "public")->
                                limit(5)->
                                orderBy("id", "desc")->
                                paginate($perPage+1)->
                                toArray();
        }



        if (is_null($curPage)) {
            $curPage = 1;
        }

        $hasNextPage = false;
        if (isset($users["data"]) && count($users["data"]) > $perPage) {
            $hasNextPage = true;
            $users["data"] = array_slice($users["data"], 0, $perPage);
        }

        if (isset($courses["data"]) && count($courses["data"]) > $perPage) {
            $hasNextPage = true;
            $courses["data"] = array_slice($courses["data"], 0, $perPage);
        }

        if (isset($groups["data"]) && count($groups["data"]) > $perPage) {
            $hasNextPage = true;
            $groups["data"] = array_slice($groups["data"], 0, $perPage);
        }

        $outPut = [ "current_page"  => (int)$curPage,
                    "per_page"      => $perPage,
                    "has_next_page" => $hasNextPage];

        if (isset($users["data"])) {
            $outPut["tutors"] = $users["data"];
        }

        if (isset($pets["data"])) {
            $outPut["pets"] = $pets["data"];
        }

        if (isset($groups["data"])) {
            $outPut["groups"] = $groups["data"];
        }

        return $outPut;
    }

    public function apiNotifications()
    {
        $user = Auth::user();
        $notifications = UserNotification:: where("notified_id",'=',$user->id)->
                                            with(["user","pet","timeline"])->
                                            orderBy('id','desc')->
                                            get();

        return $notifications;
    }

    public function apiBlockUser(Request $request)
    {
        $user = Auth::user();
        $user_id = $request->input("user_id");

        if ($user->id == $user_id) {
            return response()->json(['message' => __("Não foi possível realizar a ação.")], $this->requestNotAcceptable);
        }

        if ($request->input("block", 0)) {
            $isBlocked = BlockedUser::  where("user_id", "=", $user->id)->
                                        where("blocked_id", "=", $user_id)->exists();
            if (!$isBlocked) {

                UserNotification::where(function($query) use ($user, $user_id)
                                    {
                                        $query->where("notified_id", "=", $user->id)->
                                                where("user_id", "=", $user_id);
                                    })->
                                    orWhere(function($query) use ($user, $user_id)
                                    {
                                        $query->where("user_id", "=", $user->id)->
                                                where("notified_id", "=", $user_id);
                                    })->delete();


                BlockedUser::create(["user_id" => $user->id, "blocked_id" => $user_id]);
            }
            return response()->json(['message' => __("Usuário bloqueado com sucesso.")]);
        } else {
            $block = BlockedUser::  where("user_id", "=", $user->id)->
                                    where("blocked_id", "=", $user_id)->first();
            if ($block) {
                $block->delete();
            }
            return response()->json(['message' => __("Usuário liberado com sucesso.")]);
        }
    }

    public function apiBlockedUsers()
    {
        return User::   select("users.*")->
        join("blocked_users","blocked_users.blocked_id","=","users.id")->
        where("blocked_users.user_id","=",Auth::user()->id)->
        get();
    }

    public function uploadFile($user, $image)
    {
        if(!empty($user->filename)) {
            Storage::delete($user->filename);
        }

        $image_thumb = Image::make($image);
        if($image_thumb->width() > $image_thumb->height()) {
            $image_thumb->heighten(300, function ($constraint) {
                $constraint->aspectRatio();
            });
        } else {
            $image_thumb->widen(300, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        $image_thumb = $image_thumb->resizeCanvas(300,300);
        $image_thumb = $image_thumb->stream();

        $fileName = $user->id.time() .".". File::extension($image->getClientOriginalName());
        $fileName = User::IMG_PATH.'/'.$fileName;

        Storage::disk('public')->put($fileName, $image_thumb->__toString(), "public");
        $user->filename = $fileName;
        $user->update();

    }

}
