<?php

namespace App\Http\Controllers;

use App\Timeline;
use Illuminate\Http\Request;
use App\Group;
use Auth;
use Storage;
use File;
use App\UserGroups;

class GroupsController extends Controller
{
    public function apiStore(Request $request)
    {
        $user   = Auth::user();
        $data   = $request->all();
        $image  = $request->file("image");

        if (!isset($data["name"])) {
            return response()->json(['message' => "Informe o nome do grupo."], $this->requestNotAcceptable);
        }

        if (isset($data["privacy"])) {
            $data["privacy"] = strtolower($data["privacy"]);
            if (strpos($data["privacy"],"pu") === false) {
                $data["privacy"] = 'private';
            } else {
                $data["privacy"] = 'public';
            }
        }

        $data["user_id"] = $user->id;
        $group = Group::create($data);
        UserGroups::create([    "group_id"  => $group->id,
                                "user_id"   => $user->id,
                                "profile"   => 1]);

        if($request->hasFile("image") &&  $image->isValid()) {
            $this->uploadFile($group, $image);
        }

        return response()->json(['group' => $group], $this->requestCreatedSuccess);
    }

    public function apiUpdate(Request $request, $id)
    {
        $user   = Auth::user();
        $data   = $request->all();
        $image  = $request->file("image");

        $group = Group::findorFail($id);

        if ($group->user_id != $user->id) {
            return response()->json(["error" => "Requisição inválida"], 400);
        }

        if (!isset($data["name"])) {
            return response()->json(['message' => "Informe o nome do grupo."], $this->requestNotAcceptable);
        }

        if($request->input("remove_image", 0)  == 1) {
            Storage::delete($group->filename);
            $group->filename = null;
            $group->update();
        }


        if (isset($data["privacy"])) {
            $data["privacy"] = strtolower($data["privacy"]);
            if (strpos($data["privacy"],"pu") === false) {
                $data["privacy"] = 'private';
            } else {
                $data["privacy"] = 'public';
            }
        }

        $group->update($data);

        if($request->hasFile("image") &&  $image->isValid()) {
            $this->uploadFile($group, $image);
        }

        return response()->json(['group' => $group]);
    }

    public function apiDestroy(Request $request, $id)
    {
        $user = Auth::user();

        $group = Group::find($id);

        if ($group->user_id != $user->id) {
            return response()->json(["error" => "Requisição inválida"], 400);
        }

        $group->delete();
        return response()->json(["message" => "Removido com sucesso"], $this->requestAcceptedSuccess);
    }


    public function apiDetail($id)
    {
        return Group::whereId($id)->with(["user","pet"])->first();
    }


    public function apiSaveMember(Request $request, $id)
    {
        $userId     = $request->input("user_id");
        $profile    = $request->input("profile",0);

        $group = Group::findOrFail($id);

        if($group->user_id == $userId) {
            $profile = 1;
        }

        $memberData = [ "user_id"   => $userId,
                        "group_id"  => $id,
                        "profile"   => $profile];

        $userMember = UserGroups::  whereUserId($userId)->
                                    whereGroupId($id)->
                                    first();
        if ($userMember) {
            $userMember->update($memberData);
        } else {
            $userMember = UserGroups::create($memberData);
        }
        return $userMember;
    }

    public function apiLeaveMember(Request $request, $id)
    {
        $user = Auth::user();
        $userGroup = UserGroups::   whereUserId($user->id)->
                                    whereGroupId($id)->
                                    first();

        $userId     = $request->input("user_id");
        if ($userGroup && ($userGroup->profile == UserGroups::kADMIN || $user->id == $userId))
        {
            $userMember = UserGroups::  whereUserId($userId)->
                                        whereGroupId($id)->
                                        first();
            if ($userMember) {
                if ($userMember->profile == 1) {
                    $admins = UserGroups::  whereGroupId($id)->
                                            whereProfile(1)->
                                            get();
                    if (count($admins) == 1) {
                        $olderMember = UserGroups:: whereGroupId($id)->
                                                    whereProfile(0)->
                                                    orderBy('created_at')->
                                                    first();
                        if($olderMember) {
                            $olderMember->profile = 1;
                            $olderMember->update();
                        }
                    }
                }

                $userMember->delete();
                return response()->json(["message" => "Removido com sucesso"], $this->requestAcceptedSuccess);
            } else {
                return response()->json(["error" => "Usuário não encontrado."], 400);
            }
        }
        return response()->json(["error" => "Requisição inválida"], 400);
    }

    public function apiDetails(Request $request, $id) {

        $group = Group::findOrFail($id);
        $members = UserGroups:: with(["user"])->
                                whereGroupId($id)->
                                limit(10)->
                                get();

        if ($group->is_member || $group->privacy == "public") {
            $posts   = Timeline::   whereGroupId($id)->
                                    with(["user","pet"])->
                                    limit(10)->
                                    orderBy("id","desc");

            $lastId = $request->input("last_post_id",0);
            if ($lastId > 0) {
                $posts = $posts->where("id","<", $lastId);

            }

            $posts = $posts->get();
        } else {
            $posts = [];
        }

        return ["group"    => $group,
                "members"   => $members,
                "posts"     => $posts];
    }

    public function apiMembers($id)
    {
        $members = UserGroups:: with(["user"])->
                                whereGroupId($id)->
                                get();
        return $members;
    }

    public function uploadFile($group, $file)
    {
        if(!empty($group->filename)) {
            Storage::delete($group->filename);
        }

        $fileName = $group->id.time() .".". File::extension($file->getClientOriginalName());
        Storage::putFileAs(Group::IMG_PATH, $file, $fileName,'public');
        $group->filename = Group::IMG_PATH."/".$fileName;
        $group->update();
    }
}
