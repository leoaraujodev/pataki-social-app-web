<?php

namespace App\Http\Controllers;

use App\Tricks;
use App\TricksGroups;
use App\TrickSteps;
use App\TrickTips;
use App\TrickUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Storage;
use File;
use Illuminate\Support\Facades\Redirect;

class TricksController extends Controller
{
    public function apiTrickUpateState(Request $request) {
        $data = $request->all();

        if (in_array($data["state"], ['completed', 'in_progress','no_progress'])) {
            $userTrick = TrickUser::whereUserId(Auth::user()->id)->
                                    whereTrickId($data["trick_id"])->
                                    first();
            if ($userTrick) {
                $userTrick->state = $data["state"];
                $userTrick->update();
            } else {
                $userTrick = TrickUser::create([    "user_id"   => Auth::user()->id,
                                                    "trick_id"  => $data["trick_id"],
                                                    "state"     => $data["state"]]);
            }
            return response()->json($userTrick, $this->requestCreatedSuccess);
        } else {
            return response()->json(['message' => "Dados inválidos"], $this->requestNotAcceptable);
        }
    }

    public function apiListGroups() {
        return TricksGroups::orderBy("order")->get();
    }

    public function apiGroupTricks($id) {
        return Tricks::whereGroupId($id)->orderBy("order")->get();
    }

    public function apiTrickDetail($id)
    {
        $trick = Tricks::with(["tips","steps",'notifications'])->findOrFail($id);
        return $trick;
    }

    public function adminGroupTricks()
    {
        $groups = TricksGroups::orderBy("order")->
                                paginate();
        return view('trick.index', compact('groups'));
    }

    public function adminGroupTricksDetail($id)
    {
        $group = TricksGroups::findOrFail($id);

        return view('trick.group-detail', compact('group'));
    }

    public function adminTricksDetail($groupId, $id)
    {
        $trick = Tricks::findOrFail($id);

        return view('trick.detail', compact('trick'));
    }

    public function adminTricksSave(Request $request)
    {
        $data = $request->all();
        if (empty($data["title"])) {
            return Redirect::back()->with('error', __("Preencha o formulário corretamente"));
        }

        $trick = Tricks::create($data);
        $image  = $request->file("image");
        if($request->hasFile("image") &&  $image->isValid()) {
            $this->uploadFile($trick, $image);
        }

        return response()->redirectToRoute('trick.detail', [$trick->group_id, $trick->id])->with('success', __("Criado com sucesso."));
    }

    public function adminTricksEdit($id, Request $request)
    {
        $trick = Tricks::findOrFail($id);
        $data = $request->all();

        $trick->desc = $data["desc"];
        $trick->title = $data["title"];
        $trick->difficulty = $data["difficulty"];
        $trick->update();

        $image  = $request->file("image");
        if($request->hasFile("image") &&  $image->isValid()) {
            $this->uploadFile($trick, $image);
        }

        return response()->redirectToRoute('trick.detail', [$trick->group_id, $trick->id])->with('success', __("Editado com sucesso."));
    }



    public function submitFormTip(Request $request)
    {
        $data = $request->all();
        $trick = Tricks::findOrFail($data["trick_id"]);

        if (empty($data["tip_id"]))
        {
            if (empty($data["tip"])) {
                return Redirect::back()->with('error', __("Preencha o formulário corretamente"));
            }

//            dd($data);
            $trickTip = TrickTips::create($data);
        } else {
            $trickTip = TrickTips::findOrFail($data["tip_id"]);
            $trickTip->update($data);
        }

        return response()->redirectToRoute('trick.detail', [$trick->group_id, $trick->id])->with('success', __("Salvo com sucesso."));
    }

    public function deleteFormTip(Request $request)
    {
        $data = $request->all();
        $trickTip = TrickTips::findOrFail($data["tip_id"]);
        $trick = Tricks::find($trickTip->trick_id);
        $trickTip->delete();

        return response()->redirectToRoute('trick.detail', [$trick->group_id, $trick->id])->with('success', __("Salvo com sucesso."));
    }



    public function submitFormStep(Request $request)
    {
        $data = $request->all();
        $trick = Tricks::findOrFail($data["trick_id"]);

        if (empty($data["step_id"]))
        {
            if (empty($data["desc"])) {
                return Redirect::back()->with('error', __("Preencha o formulário corretamente"));
            }

            $trickStep = TrickSteps::create($data);
        } else {
            $trickStep = TrickSteps::findOrFail($data["step_id"]);
            $trickStep->update($data);
        }

        $image  = $request->file("image");
        if($request->hasFile("image") &&  $image->isValid()) {
            $this->uploadStepFile($trickStep, $image);
        }

        return response()->redirectToRoute('trick.detail', [$trick->group_id, $trick->id])->with('success', __("Salvo com sucesso."));
    }

    public function deleteFormStep(Request $request)
    {
        $data = $request->all();
        $trickStep = TrickSteps::findOrFail($data["step_id"]);
        if(!empty($trickStep->file)) {
            Storage::delete($trickStep->file);
        }
        $trickStep->delete();

        $trick = Tricks::find($trickStep->trick_id);

        return response()->redirectToRoute('trick.detail', [$trick->group_id, $trick->id])->with('success', __("Salvo com sucesso."));
        dd($data);
    }

    public function uploadStepFile($trickStep, $file)
    {
        if(!empty($trickStep->file)) {
            Storage::delete($trickStep->file);
        }

        $fileName = $trickStep->id.time().".".File::extension($file->getClientOriginalName());
        Storage::putFileAs(TrickSteps::IMG_PATH, $file, $fileName,'public');
        $trickStep->file = TrickSteps::IMG_PATH."/".$fileName;
        $trickStep->update();
    }

    public function uploadFile($trick, $file)
    {
        if(!empty($trick->file)) {
            Storage::delete($trick->file);
        }

        $fileName = $trick->id.time().".".File::extension($file->getClientOriginalName());
        Storage::putFileAs(Tricks::IMG_PATH, $file, $fileName,'public');
        $trick->file = Tricks::IMG_PATH."/".$fileName;
        $trick->update();
    }
}
