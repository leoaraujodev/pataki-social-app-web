<?php

namespace App\Http\Controllers;

use App\ChatMessage;
use App\ChatRoom;
use App\ChatSticker;
use App\OneSignalNotification;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Storage;
use File;

class ChatController extends Controller
{
    public function apiListStickers() {
        return ChatSticker::all();
    }

    public function apiListRooms()
    {
        $myId = Auth::user()->id;

        $blockedUsers = Auth::user()->blockedUserIds();

        $rooms = ChatRoom::  with(["lastMessage","lastMessage.sticker"])->
                            whereNotNull("last_message_id")->
                            where(function($query) use ($myId)
                            {
                                $query->where('user1_id','=',$myId)->
                                        orWhere("user2_id",'=', $myId);
                            })->
                            get();

        $outPut = [];

        foreach ($rooms as $room) {
            if(!in_array($room->contact->id, $blockedUsers)) {
                $outPut[] = $room;
            }
        }
        return $outPut;
    }

    public function apiRoom(Request $request)
    {
        $limit      = 25;
        $userId     = $request->input("user_id", -1);
        $lastId     = $request->input("last_id", -1);
        $firstId    = $request->input("first_id", -1);


        $user = User::findOrFail($userId);
        if($user->is_blocked) {
            return response()->json(null, 404);
        }


        $chatRoom = ChatRoom::chatRoomWith($user->id);
        $messages = ChatMessage::   with(["sticker"])->
                                    where("room_id","=", $chatRoom->id)->
                                    orderBy("id","desc");

        if ($lastId > 0) {
            $messages = $messages->where("id", ">", $lastId);
        } else {
            if ($firstId > 0) {
                $messages = $messages->where("id","<", $firstId);
            }
            $messages = $messages->limit($limit);
        }

        $messages = $messages->get();

        $outPut = [ "room"      => $chatRoom,
                    "messages"  => $messages];
        return $outPut;
    }

    public function apiPostMessage(Request $request)
    {
        $data = $request->all();
        $chatRoom = ChatRoom::find($data["room_id"]);
        if($chatRoom->contact->is_blocked) {
            return response()->json(null, 404);
        }

        $pushMessage = "Enviou uma mensagem.";
        if (!$chatRoom) {
            return response()->json(['message' => "Conversa não encontrada."], $this->requestNotAcceptable);
        }
        if (empty($data["message"]) &&
            empty($data["sticker_id"]) &&
            empty($data["image"])) {
            return response()->json(['message' => "Mensagem inválida"], $this->requestNotAcceptable);
        }

        $messageData = ["room_id"   => $data["room_id"],
                        "sender_id" => Auth::user()->id];

        if ($request->has("sticker_id")) {
            $chatSticker = ChatSticker::findOrFail($data["sticker_id"]);
            if ($chatSticker) {
                $pushMessage = "Enviou uma imagem.";
                $messageData["sticker_id"] = $chatSticker->id;
            }
        }

        if ($request->has("message")) {
            $pushMessage = $messageData["message"] = $data["message"];

            if (strlen($pushMessage) > 50)
                $pushMessage = substr($pushMessage, 0, 45) . '...';
        }

        $message = ChatMessage::create($messageData);
        $a = $message->sticker;

        $image  = $request->file("image");
        if($request->hasFile("image") &&  $image->isValid()) {
            $this->uploadFile($message, $image);
            $pushMessage = "Enviou uma imagem.";
        }

        $chatRoom->last_message_id = $message->id;
        $chatRoom->update();

        if (!is_null($chatRoom->contact->onesiginal_token))
        {
            OneSignalNotification::sendPushNotification($pushMessage,
                                                        $chatRoom->contact->onesiginal_token,
                                                        null,
                                                        null,
                                                        Auth::user()->name);
        }

        return $message;
    }

    public function uploadFile($message, $file)
    {
        if(!empty($message->file)) {
            Storage::delete($message->file);
        }

        $fileName = $message->id.time().".".File::extension($file->getClientOriginalName());
        Storage::putFileAs(ChatMessage::IMG_PATH, $file, $fileName,'public');
        $message->file = ChatMessage::IMG_PATH."/".$fileName;
        $message->update();
    }
}
