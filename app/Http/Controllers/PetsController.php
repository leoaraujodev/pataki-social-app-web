<?php

namespace App\Http\Controllers;

use App\Pet;
use App\PetBreed;
use App\PetTypes;
use App\Timeline;
use App\User;
use App\UserFollow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Storage;
use File;
use Validator;
use App\TimelineReport;

class PetsController extends Controller
{
    public function apiTypeList()
    {
        return PetTypes::all();
    }

    public function apiBreedList($id)
    {
        return PetBreed::   where("type_id","=",$id)->
                            orderBy("sort")->
                            orderBy("name")->
                            get();
    }

    public function apiUserFollower($id)
    {

        $pet = Pet::findOrFail($id);

        if($pet->tutor->is_blocked) {
            return response()->json(null, 404);
        }

        $userFollowed = User::  select("users.*")->
                                leftJoin('user_follows', 'users.id', '=', 'user_follows.user_id')->
                                where("pet_id", "=", $id)->
                                get();
        return $userFollowed;
    }

    public function apiPosts($id, Request $request)
    {
        $user = Auth::user();

        $pet = Pet::findOrFail($id);

        if($pet->tutor->is_blocked) {
            return response()->json(null, 404);
        }

        $reportedPosts = TimelineReport::   where("user_id", "=", $user->id)->
                                            pluck("timeline_id")->
                                            toArray();

        $timeline = Timeline::  with(["user",'pet'])->
                                wherePetId($id)->
                                whereNotIn("id", $reportedPosts)->
                                whereNull("group_id")->
                                orderBy("id","desc")->
                                limit(15);

        $lastId = $request->input("last_id",0);
        if ($lastId > 0) {
            $timeline = $timeline->where("id","<", $lastId);

        }

        $output = $timeline->get();
        return ["data"=> $output];
    }

    public function apiCreatePet(Request $request)
    {
        $data = $request->all();

        $rules = [
            'name'      => 'required',
        ];

        $validator = Validator::make($data,$rules);

        $error = "";
        if ($validator->fails()) {
            $messages = $validator->messages()->all();
            return response()->json(['message' => $messages[0]], $this->requestNotAcceptable);
        } else {
            $data["user_id"] = Auth::user()->id;
            $pet = Pet::create($data);
            if ($pet) {
                $image  = $request->file("picture");
                if($request->hasFile("picture") &&  $image->isValid()) {
                    $this->uploadFile($pet, $image);
                }
                return response()->json($pet);
            }
        }
        return response()->json(['message' => "Invalid Request"], $this->requestNotAcceptable);
    }

    public function apiSearchTag(Request $request)
    {
        $tag = $request->input("tag");

        $pet = Pet::where("tag","=",$tag)->
                    with("tutor")->
                    first();
        if (!$pet) {
            return response()->json(['message' => "No result"], $this->requestNoContent);
        }
        if($pet->tutor->is_blocked) {
            return response()->json(null, 404);
        }

        return $pet;
    }

    public function apiEditPet(Request $request)
    {
        $pet = Pet::findOrFail($request->input("id"));
        if ($pet->user_id == Auth::user()->id) {
            $data = $request->all();

            if($request->has("tag")) {
                $rules = [
                    'tag'      => 'required',
                ];
            } else {
                $rules = [
                    'name'      => 'required',
                ];
            }

            $validator = Validator::make($data,$rules);

            if ($validator->fails()) {
                $messages = $validator->messages()->all();
                if (count($messages) > 1) {
                    return response()->json(['messages' => $messages], $this->requestNotAcceptable);
                } else {
                    return response()->json(['message' => $messages[0]], $this->requestNotAcceptable);
                }
            } else {
                $pet->update($data);
                $image  = $request->file("picture");
                if($request->hasFile("picture") &&  $image->isValid()) {
                    $this->uploadFile($pet, $image);
                }
                return response()->json($pet);
            }
        }

        return response()->json(['message' => "Invalid Request"], $this->requestNotAcceptable);
    }

    public function apiDetails($id)
    {
        $pet = Pet::findOrFail($id);

        if($pet->tutor->is_blocked) {
            return response()->json(null, 404);
        }

        $pet->tutor;
        $pet->breed;
        $pet->breed->type;



        $petArr = $pet->toArray();
        $petArr["tot_followers"] = UserFollow::wherePetId($id)->count();
        return $petArr;
    }

    public function apiDeletePet(Request $request)
    {
        $pet = Pet::findOrFail($request->input("id"));
        if ($pet->user_id == Auth::user()->id) {
            foreach ($pet->vaccines as $vaccine) {
                $vaccine->delete();
            }

            foreach ($pet->notifications as $notification) {
                $notification->delete();
            }

            $pet->delete();
            return response()->json(['message' => "Removido com sucesso."]);
        }
        return response()->json(['message' => "Invalid Request"], $this->requestNotAcceptable);
    }

    public function uploadFile($pet, $image)
    {
        if(!empty($pet->filename)) {
            Storage::delete($pet->filename);
        }

        $image_thumb = Image::make($image);
        if($image_thumb->width() > $image_thumb->height()) {
            $image_thumb->heighten(300, function ($constraint) {
                $constraint->aspectRatio();
            });
        } else {
            $image_thumb->widen(300, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        $image_thumb = $image_thumb->resizeCanvas(300,300);
        $image_thumb = $image_thumb->stream();

        $fileName = $pet->id.time() .".". File::extension($image->getClientOriginalName());
        $fileName = Pet::IMG_PATH.'/'.$fileName;

        Storage::disk('public')->put($fileName, $image_thumb->__toString(), "public");
        $pet->filename = $fileName;
        $pet->update();

    }
}
