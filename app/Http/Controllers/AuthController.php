<?php

namespace App\Http\Controllers;

use App\Pet;
use App\PetNotification;
use App\Vaccine;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use JWTAuth;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','refresh']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        try {
            $token = JWTAuth::attempt($credentials);
        } catch (Exception $e) {
            return response()->json(["error"=>"Ocorreu um erro inesperado. Tente novamente mais tarde."], 500);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        $token = JWTAuth::getToken();
        $new_token = JWTAuth::refresh($token);
        return response()->json(['access_token' => $new_token]);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        $user = JWTAuth::user();
        $myPets = Pet::whereUserId($user->id)->pluck("id")->toArray();

        $notifications  = PetNotification::with(['pet'])->whereIn("pet_id", $myPets)->get();
        $petsVaccines   = Vaccine::with(['pet'])->whereIn("pet_id", $myPets)->get();
        $notificationsTricks  = PetNotification::with(['trick'])->where("user_id", '=',$user->id)->get();


        if ($token) {
            return response()->json([
                'access_token' => $token,
                'user'  => $user,
                'pets_notifications'=> $notifications,
                'pets_vaccines' => $petsVaccines,
                'tricks_notifications' => $notificationsTricks,
//            'token_type' => 'bearer',
//            'expires_in' => auth()->factory()->getTTL() * 60
            ]);
        } else {
            return response()->json(['message' =>  __("Usuário/Email não encontrado.")], 401);
        }


    }
}
