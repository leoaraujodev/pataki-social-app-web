<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    //REQUESTS SUCCESS
    protected $requestCreatedSuccess        = 201;
    protected $requestAcceptedSuccess       = 202;
    protected $requestNoContent             = 204;

    //REQUESTS ERRORS
    protected $requestNotAcceptable         = 406;
    protected $requestInternalServerError   = 500;
}
