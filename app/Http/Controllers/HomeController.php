<?php

namespace App\Http\Controllers;

use App\Timeline;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function home()
    {
        $now = Carbon::now();
        $date30dAgo = $now->subDay(30);


        $countNewUsers       = User::where("created_at",">", $date30dAgo)->count();
        $countNewPosts       = Timeline::where("created_at",">", $date30dAgo)->count();

        return view('home', compact('countNewUsers','countNewPosts'));
    }
}
