<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        if (Auth::attempt(['email' => strtolower($request->input("email")), 'password' => $request->input("password")])) {
            if(Auth::user()->is_admin) {
                return redirect()->intended('/home');
            } else {
                Auth::logout();
                return redirect('/login')->with('error', __("No Access"));
            }


        } else {
            return response()->redirectToRoute('login')->with('error', __("Usuário/Email não encontrado."));
        }
    }

    /*
     *  public function login(Request $request)
    {
        if (Auth::attempt(['email' => strtolower($request->input("email")), 'password' => $request->input("password")])) {
            if(Auth::user()->isAdmin()) {
                return redirect('/dashboard');
            } else {
                dd("e");
                Auth::logout();
                return redirect('/login')->with('error', __("No Access"));
            }


        } else {
//            dd("ee");
            return response()->redirectToRoute('login')->with('error', __("Usuário/Email não encontrado."));
        }
    }
     *
     */

    protected function credentials()
    {
        $username = $this->username();
        $credentials = request()->only($username, 'password');
        if (isset($credentials[$username])) {
            $credentials[$username] = strtolower($credentials[$username]);
        }
        return $credentials;
    }
}
