<?php

namespace App\Http\Controllers;

use App\CoursePrices;
use App\CourseRates;
use App\CourseSignature;
use Illuminate\Http\Request;
use App\Course;
use Auth;
use File;
use Storage;
use App\User;
use App\Meditation;

class CoursesController extends Controller
{

    public function index()
    {
        $courses = Course:: orderBy('name')->
                            paginate();
        return view('course.index', compact('courses'));
    }


    public function apiStore(Request $request)
    {
        $user   = Auth::user();
        $data   = $request->all();
        $image  = $request->file("image");

        if (!isset($data["name"])) {
            return response()->json(['message' => "Informe o nome do grupo."], $this->requestNotAcceptable);
        }

        $data["user_id"] = $user->id;
        $course = Course::create($data);


        if($request->hasFile("image") &&  $image->isValid()) {
            $this->uploadFile($course, $image);
        }

        return response()->json(['course' => $course], $this->requestCreatedSuccess);
    }

    public function apiUpdate(Request $request, $id)
    {
        $user   = Auth::user();
        $data   = $request->all();
        $image  = $request->file("image");

        $course = Course::find($id);
        if ($course->user_id != $user->id) {
            return response()->json(["error" => "Requisição inválida"], 400);
        }

        if (!isset($data["name"])) {
            return response()->json(['message' => "Informe o nome do grupo."], $this->requestNotAcceptable);
        }

        $course->update($data);

        if($request->hasFile("image") &&  $image->isValid()) {
            $this->uploadFile($course, $image);
        }

        return response()->json(['course' => $course]);
    }

    public function apiDestroy(Request $request, $id)
    {
        $user = Auth::user();

        $course = Course::find($id);

        if ($course->user_id != $user->id) {
            return response()->json(["error" => "Requisição inválida"], 400);
        }

        $course->delete();
        return response()->json(["message" => "Removido com sucesso"], $this->requestAcceptedSuccess);
    }

    public function apiSignature(Request $request, $id)
    {
        $user = Auth::user();
        $course = Course::findOrFail($id);

        if ($course->user_id == $user->id) {
            return response()->json(["error" => "Requisição inválida"], 400);
        }

        if (CourseSignature::whereUserId($user->id)->whereCourseId($course->id)->exists()) {
            return response()->json(["error" => "Curso já está assinado."], 400);
        }

        CourseSignature::create(["course_id"=>$course->id, "user_id"=>$user->id]);

        return response()->json(["message" => "Assinado com sucesso."], $this->requestAcceptedSuccess);
    }

    public function apiRate(Request $request, $id)
    {
        $user   = Auth::user();
        $course = Course::findOrFail($id);

        $signed = CourseSignature::whereUserId($user->id)->whereCourseId($course->id)->exists();
        if ($user->id != $course->user_id && !$signed)
        {
            return response()->json(["error" => "Requisição inválida"], 400);
        }

        $rate = CourseRates::whereUserId($user->id)->whereCourseId($course->id)->first();
        if ($rate) {
            $rate->delete();
        }

        $data = [   "user_id"   => $user->id,
                    "course_id" => $course->id,
                    "rate"      => $request->input("rate"),
                    "comment"   => $request->input("comment")];

        CourseRates::create($data);
        $course->average_rates = CourseRates::whereCourseId($course->id)->avg('rate');
        $course->update();
        return response()->json(["message" => "Registrado com sucesso."], $this->requestAcceptedSuccess);
    }

    public function apiDetails(Request $request, $id)
    {
        $user            = Auth::user();
        $course          = Course::findOrFail($id);
        $course->user;
        $course->price;
        $data["course"]     = $course->toArray();
        $data["subscribed"] = CourseSignature::whereCourseId($id)->whereUserId($user->id)->exists();
        $data["course"]["tot_subscribers"] = User:: select("users.*")->
                                                    join("course_signatures","users.id","=","user_id")->
                                                    where("course_signatures.course_id","=", $id)->
                                                    count();

        $data["evaluations"]  = CourseRates::with(["user"])->whereCourseId($id)->get();
        $data["meditations"]  = Meditation::whereCourseId($id)->get();
        return $data;
    }

    public function apiPayments()
    {
        return CoursePrices::get();
    }

    public function uploadFile($course, $file)
    {
        if(!empty($course->filename)) {
            Storage::delete($course->filename);
        }

        $fileName = $course->id.time() .".". File::extension($file->getClientOriginalName());
        Storage::putFileAs(Course::IMG_PATH, $file, $fileName,'public');
        $course->filename = Course::IMG_PATH."/".$fileName;
        $course->update();
    }
}
