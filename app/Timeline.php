<?php

namespace App;

use App\Comment;
use App\TimelineLike;
use Illuminate\Database\Eloquent\Model;
use App\Tag;
use Storage;
use Auth;

class Timeline extends Model
{
    const IMG_PATH = "timeline";

    protected $fillable = [
        "hide",'desc', 'user_id', 'tot_likes', 'tot_comment', 'filename', 'pet_id', 'group_id'
    ];

    protected $hidden   = ["filename","hide",'pet_id','group_id'];
    protected $appends  = ["image_path","liked","last_comment", "mention_pets"];

    public function getMentionPetsAttribute()
    {
        $pets = [];
        preg_match_all("|<PTK-P+>(.*)</PTK-P+>|U", $this->desc, $out, PREG_PATTERN_ORDER);
        $mentions = @$out[1];
        if (is_null($mentions) || count($mentions) == 0) {
            return $pets;
        }


        foreach ($mentions as $mention) {
            $mentionData = explode("|",$mention);
            $pet = Pet::find($mentionData[0]);
            if ($pet) {
                $pets[] = $pet;
            }
        }
        return $pets;
    }

    public function getLastCommentAttribute()
    {
        return Comment::    where("timeline_id", "=", $this->id)->
                            with("user")->
                            orderBy("id","desc")->
                            limit(1)->first();
        return TimelineLike::whereUserId(Auth::user()->id)->whereTimelineId($this->id)->exists();
    }

    public function getLikedAttribute()
    {
        return TimelineLike::whereUserId(Auth::user()->id)->whereTimelineId($this->id)->exists();
    }

    public function getImagePathAttribute(){
        if(empty($this->filename)) {
            return;
        }
        return Storage::url($this->filename);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'table_timeline_tags', 'timeline_id', 'tag_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function pet()
    {
        return $this->belongsTo(Pet::class, 'pet_id');
    }

    private static function syncTags($timeline) {
        TimelineTags::where("timeline_id","=", $timeline->id)->delete();
        preg_match_all("/(#\w+)/", $timeline->desc, $tags);
        if (count($tags) == 2) {
            $tags = Tag::stringToTags($tags[0]);
            foreach ($tags as $tag) {
                TimelineTags::create([  "tag_id"=>$tag->id,
                                        "timeline_id" => $timeline->id]);
            }
        }
    }

    //OVERWRITE METHODS
    public static function create(array $attributes = [])
    {
        $model = static::query()->create($attributes);

        Timeline::syncTags($model);

        return $model;
    }

    public function update(array $attributes = array(), array $options = array())
    {
        if ( ! $this->exists)
        {
            $obj = $this->newQuery()->update($attributes, $options);
            Timeline::syncTags($this);
            return $obj;
        }

        $obj = $this->fill($attributes)->save();
        Timeline::syncTags($this);
        return $obj;
    }

    public function delete()
    {
        TimelineTags::where("timeline_id", "=", $this->id)->delete();
        TimelineLike::where("timeline_id", "=", $this->id)->delete();
        TimelineReport::where("timeline_id", "=", $this->id)->delete();
        UserNotification::where("timeline_id", "=", $this->id)->delete();

        foreach (Comment::where("timeline_id", "=", $this->id)->get() as $comment) {
            $comment->delete();
        }

        if(!empty($this->filename)) {
            Storage::delete($this->filename);
        }

        return parent::delete();
    }
}
