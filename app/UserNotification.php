<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNotification extends Model
{
    protected $fillable = ['action','notified_id','pet_id','user_id','timeline_id',"message"];
    protected $hidden = ["notified_id","updated_at",'pet_id','user_id','timeline_id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function timeline()
    {
        return $this->belongsTo(Timeline::class, 'timeline_id');
    }

    public function pet()
    {
        return $this->belongsTo(Pet::class, 'pet_id');
    }

}
