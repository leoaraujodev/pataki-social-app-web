<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Storage;

class Tricks extends Model
{

    const IMG_PATH = "tricks";

    protected $fillable = ["group_id","title","desc","difficulty","file","order"];

    protected $hidden = ["group_id","order"];
    protected $appends  = ["image_path", 'state'];
    public $timestamps = false;

    public function getStateAttribute()
    {
        $userTrick = TrickUser::whereUserId(Auth::user()->id)->whereTrickId($this->id)->first();
        if ($userTrick) {
            return $userTrick->state;
        }
        return 'no_progress';
    }

    public function getImagePathAttribute(){
        if(empty($this->file)) {
            return;
        }
        return Storage::url($this->file);
    }



    public function group()
    {
        return $this->belongsTo(TricksGroups::class, "group_id");
    }

    public function notifications()
    {
        return $this->hasMany(PetNotification::class, "trick_id");
    }

    public function tips() {
        return $this->hasMany(TrickTips::class, "trick_id")->orderBy("order");
    }

    public function steps() {
        return $this->hasMany(TrickSteps::class, "trick_id")->orderBy("order");
    }
}
