<?php

namespace App;

use App\UserTags;
use App\Tag;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Storage;
use App\BlockedUser;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    const IMG_PATH = "user/image";

    protected $fillable = [
        'name', 'email', 'password', 'onesiginal_token','desc','phone','is_admin'
    ];

    protected $hidden = [
        'password', 'remember_token','notify_token','email_verified_at','filename'
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = ["image_path","is_blocked"];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getIsBlockedAttribute() {
        if ($this->id == Auth::user()->id) {
            return false;
        }

        return BlockedUser::where(function($query)
                            {
                                $query->where("blocked_id", "=", $this->id)->
                                        where("user_id", "=", Auth::user()->id);
                            })->
                            orWhere(function($query)
                            {
                                $query->where("user_id", "=", $this->id)->
                                        where("blocked_id", "=", Auth::user()->id);
                            })->
                            exists();

    }

    function blockedUserIds()
    {
        $blocks = BlockedUser::where("user_id", '=', $this->id)->orWhere("blocked_id", "=", $this->id)->get();
        $blockIds = [];
        foreach ($blocks as $block) {
            if($block->user_id == $this->id) {
                $blockIds[] = $block->blocked_id;
            } else{
                $blockIds[] = $block->user_id;
            }
        }
        return $blockIds;
    }

    public function getFollowedAttribute() {
        if ($this->id == Auth::user()->id) {
            return false;
        }
        return UserFollow:: where("user_id", "=", Auth::user()->id)->
                            where("followed_id", "=", $this->id)->exists();
    }

    static function byUUID($uuid)
    {
        return User::  where("uuid","=", $uuid)->
                        first();
    }

    public function getImagePathAttribute() {
        if ($this->filename) {
            return Storage::url($this->filename);
        }
        return "https://app.pataki.com.br/img/user-placeholder.png";
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function addTagRelation($tags)
    {
        foreach ($tags as $tag) {
            $userTag = UserTags::   where("user_id", "=", $this->id)->
                                    where("tag_id", "=", $tag->id)->
                                    first();
            if ($userTag) {
                $userTag->count++;
                $userTag->update();
            } else {
                UserTags::create(["user_id"=> $this->id, "tag_id"=>$tag->id]);
            }
        }
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'user_tags', 'user_id', 'tag_id')->orderBy("count","desc")->limit(5);
    }

    public function courses()
    {
        return $this->hasMany(Course::class, 'user_id');
    }

    public function delete()
    {

        TimelineLike::where("user_id", "=", $this->id)->delete();
        TimelineReport::where("user_id", "=", $this->id)->delete();
        UserNotification::where("user_id", "=", $this->id)->delete();

        foreach (Comment::where("user_id", "=", $this->id)->get() as $comment) {
            $comment->delete();
        }

        foreach (Timeline::where("user_id", "=", $this->id)->get() as $obj) {
            $obj->delete();
        }

        foreach (Pet::where("user_id", "=", $this->id)->get() as $obj) {
            $obj->delete();
        }

        foreach (UserInvite::where("user_id", "=", $this->id)->get() as $obj) {
            $obj->delete();
        }

        foreach (UserFollow::where("user_id", "=", $this->id)->get() as $obj) {
            $obj->delete();
        }

        foreach (UserGroups::where("user_id", "=", $this->id)->get() as $obj) {
            $obj->delete();
        }

        foreach (UserReport::where("user_id", "=", $this->id)->get() as $obj) {
            $obj->delete();
        }

        foreach (UserReport::where("reported_id", "=", $this->id)->get() as $obj) {
            $obj->delete();
        }

        if(!empty($this->filename)) {
            Storage::delete($this->filename);
        }

        return parent::delete();
    }
}
