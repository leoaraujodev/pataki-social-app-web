<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Storage;
use App\UserGroups;

class Group extends Model
{
    const IMG_PATH = "groups";

    protected $fillable = [
        'desc', 'name', 'filename', 'privacy', 'user_id'
    ];

    protected $hidden   = ["filename",'user_id'];
    protected $appends  = ["image_path","tot_members",'is_member', "last_members"];


    public function getLastMembersAttribute() {
        return UserGroups:: with("user")->
                            whereGroupId($this->id)->
                            limit(3)->
                            orderBy('created_at')->
                            get();
    }

    public function getTotMembersAttribute(){
        return UserGroups::whereGroupId($this->id)->count();
    }



    public function getIsMemberAttribute()
    {
        $userId = Auth::user()->id;
        return UserGroups:: whereGroupId($this->id)->
                            whereUserId($userId)->
                            exists();
    }

    public function getImagePathAttribute(){
        if(empty($this->filename)) {
            return "https://app.pataki.com.br/img/pataki-logo.png";
        }
        return Storage::url($this->filename);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function delete()
    {
        if(!empty($this->filename)) {
            Storage::delete($this->filename);
        }

        UserGroups::    where("group_id", "=", $this->id)->
                        delete();

        return parent::delete();
    }
}
