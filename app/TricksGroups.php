<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TricksGroups extends Model
{
    protected $fillable = ["name","desc","order"];

    protected $hidden = ["created_at","updated_at","order"];

    public function tricks() {
        return $this->hasMany(Tricks::class,"group_id");
    }
}
