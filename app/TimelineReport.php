<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimelineReport extends Model
{
   protected $fillable = ["user_id","timeline_id","reason","comment"];

    public function timeline()
    {
        return $this->belongsTo(Timeline::class, 'timeline_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
