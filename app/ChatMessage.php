<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;

class ChatMessage extends Model
{
    const IMG_PATH = "chat";

    protected $fillable = [ "room_id",
                            "sender_id" ,
                            "message",
                            "file",
                            "sticker_id"];

    protected $hidden = ["updated_at", "sticker_id", "file", "room_id"];
    protected $appends  = ["image_path"];

    public function sticker()
    {
        return $this->belongsTo(ChatSticker::class, 'sticker_id');
    }

    public function getImagePathAttribute(){
        if(empty($this->file)) {
            return;
        }
        return Storage::url($this->file);
    }
}
