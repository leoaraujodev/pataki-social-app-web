<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Message;
use App\Notification;

class OneSignalNotification extends Model
{
    public static function sendPushNotification($message, $tokens = null, $data = null, $webUrl = null, $title = null) {
        $content = array(
            "en" => strip_tags($message)
        );

        $fields = array(
            'app_id' => "f204f28d-5554-438f-9949-645cbfc1b9c8",
            'contents' => $content,
            'content_available'=>1,
        );

        if(!is_null($data)) {
            $fields["data"] = $data;
        }

        if (!is_null($title)) {
            $fields["headings"] = ["en" => $title];
            $fields["subtitle"] = ["en" => ""];
        }

        if (!is_null($webUrl)) {
            $fields["web_url"] = $webUrl;
        }

        if(is_null($tokens)) {
            $fields["included_segments"] = array('All');
        } else {
            if(is_array($tokens)) {
                $fields["include_player_ids"] = $tokens;
            } else {
                $fields["include_player_ids"] = array($tokens);
            }
        }

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
            'Authorization: Basic MmI2YzliNmMtZmUxZi00YTkzLTlkNTItNzJlZWI4MjNkYzJh'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }
}
