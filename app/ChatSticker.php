<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatSticker extends Model
{
    protected $fillable = ["file"];

    protected $hidden = ["created_at", "updated_at", "file"];

    protected $appends = ["path"];

    public function getPathAttribute()
    {
        return asset("img/".$this->file);
    }
}
