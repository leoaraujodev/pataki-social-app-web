<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Timeline;
use App\TimelineLike;
use Auth;

class Comment extends Model
{
    protected $fillable = ["user_id", "timeline_id", "parent_id", "comment", "tot_likes", "meditation_id"];
    protected $appends  = ["liked", "mention_pets"];
    protected $hidden = ["parent_id","user_id","timeline_id","meditation_id","updated_at"];

    public function getMentionPetsAttribute()
    {
        $pets = [];
        preg_match_all("|<PTK-P+>(.*)</PTK-P+>|U",
            $this->comment,
            $out, PREG_PATTERN_ORDER);
        $mentions = @$out[1];
        if (is_null($mentions) || count($mentions) == 0) {
            return $pets;
        }


        foreach ($mentions as $mention) {
            $mentionData = explode("|",$mention);
            $pet = Pet::find($mentionData[0]);
            if ($pet) {
                $pets[] = $pet;
            }
        }
        return $pets;
    }

    public function getLikedAttribute()
    {
        return CommentLike::whereUserId(Auth::user()->id)->whereCommentId($this->id)->exists();
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function timeline()
    {
        return $this->belongsTo(Timeline::class, 'timeline_id');
    }

    //OVERWRITE METHODS
    public static function create(array $attributes = [])
    {
        $model = static::query()->create($attributes);

        if ((int)$model->timeline_id > 0) {
            $timeline = Timeline::findOrFail($model->timeline_id);
            $timeline->tot_comment++;
            $timeline->update();
        }

        return $model;
    }


    public function delete()
    {
        if ((int)$this->timeline_id > 0) {
            $timeline = Timeline::findOrFail($this->timeline_id);
            $timeline->tot_comment--;
            $timeline->update();
        }
        CommentLike::where("comment_id", "=", $this->id)->delete();
        return parent::delete();
    }
}
