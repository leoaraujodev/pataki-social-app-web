<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Timeline;
use App\UserFollow;


class Pet extends Model
{
    const IMG_PATH = "pet/image";

    protected $fillable = ["name", "filename", "size", "weight", "user_id", "breed_id", "birth_date", 'tag'];

    protected $hidden = ["filename","user_id"];
    protected $appends = ["image_path","followed"];

    public function getImagePathAttribute() {
        if ($this->filename) {
            return Storage::url($this->filename);
        }
        return $this->breed->type->defaultImg();
    }

    public function getFollowedAttribute() {
        if ($this->user_id == Auth::user()->id) {
            return true;
        }
        return UserFollow:: where("user_id", "=", Auth::user()->id)->
                            where("pet_id", "=", $this->id)->exists();
    }

    public function delete()
    {
        if(!empty($this->filename)) {
            Storage::delete($this->filename);
        }

        UserNotification::where("pet_id", "=", $this->id)->delete();
        UserFollow::where("pet_id", "=", $this->id)->delete();
        foreach (Timeline::where("pet_id", "=", $this->id)->get() as $post) {
            $post->delete();
        }

        return parent::delete();
    }

    public function tutor()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function breed()
    {
        return $this->belongsTo(PetBreed::class, 'breed_id');
    }

    public function vaccines()
    {
        return $this->hasMany(Vaccine::class, 'pet_id')->orderBy("date");
    }

    public function notifications()
    {
        return $this->hasMany(PetNotification::class, 'pet_id');
    }
}
