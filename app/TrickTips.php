<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrickTips extends Model
{
    protected $fillable = ["tip","order","trick_id"];
    protected $hidden = ["order","trick_id"];

    public $timestamps = false;


}
