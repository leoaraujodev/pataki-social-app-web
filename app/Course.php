<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;

class Course extends Model
{
    const IMG_PATH = "courses";

    protected $fillable = [
        'name', 'desc', 'user_id', 'joothi_perc', 'filename','average_rates','price_id'
    ];

    protected $hidden   = ["filename",'user_id','joothi_perc'];
    protected $appends  = ["image_path","tot_meditations"];

    public function getTotMeditationsAttribute() {
        return Meditation::whereCourseId($this->id)->count();
    }

    public function getImagePathAttribute(){
        if(empty($this->filename)) {
            return;
        }
        return Storage::url($this->filename);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function price()
    {
        return $this->belongsTo(CoursePrices::class, 'price_id');
    }

    public function signatures()
    {
        return $this->hasMany(CourseSignature::class, 'course_id');
    }

    public function delete()
    {
        if(!empty($this->filename)) {
            Storage::delete($this->filename);
        }
        return parent::delete();
    }
}
