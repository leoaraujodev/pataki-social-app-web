<?php

namespace App;
use Auth;

use Illuminate\Database\Eloquent\Model;

class ChatRoom extends Model
{
    protected $fillable = [ "last_message_id",
                            "user1_id" ,
                            "user2_id"];

    protected $hidden   = ["updated_at", "user1_id", "user2_id", "last_message_id"];
    protected $appends  = ["contact"];

    public function getContactAttribute()
    {
        $myId = Auth::user()->id;
        if ($myId == $this->user1_id) {
            return User::find($this->user2_id);
        } else {
            return User::find($this->user1_id);
        }
    }

    static function chatRoomWith($userId)
    {
        $myId = Auth::user()->id;

        $room = ChatRoom::  where(function($query) use ($myId, $userId)
                            {
                                $query->where('user1_id','=',$myId)->
                                        where("user2_id",'=', $userId);
                            })->
                            orWhere(function($query) use ($myId, $userId)
                            {
                                $query->where('user2_id','=',$myId)->
                                        where("user1_id",'=', $userId);
                            })->
                            first();

        if ($room) {
            return $room;
        }

        return ChatRoom::create(["user2_id"=> $myId,"user1_id"=>$userId]);
    }

    public function lastMessage()
    {
        return $this->belongsTo(ChatMessage::class, 'last_message_id');
    }
}
