<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PetBreed extends Model
{
    protected $fillable = ["type_id","name","sort"];
    protected $hidden = ["created_at","updated_at","type_id","sort"];


    public function type()
    {
        return $this->belongsTo(PetTypes::class, 'type_id');
    }
}
