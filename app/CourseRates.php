<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseRates extends Model
{
    protected $fillable = ["user_id","course_id","rate","comment"];
    protected $hidden   = ["course_id","updated_at","user_id"];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
