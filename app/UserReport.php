<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserReport extends Model
{
    protected $fillable = ["user_id","reported_id","reason","comment"];

    public function reported()
    {
        return $this->belongsTo(User::class, 'reported_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
