<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tag;

class TimelineTags extends Model
{
    protected  $table = "timeline_tags";
    protected $fillable = ["timeline_id", "tag_id"];

    public function tag()
    {
        return $this->belongsTo(Tag::class, 'tag_id');
    }
}
