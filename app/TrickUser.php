<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrickUser extends Model
{
    protected $fillable = ["state", "user_id", "trick_id"];
    public $timestamps = false;
}
