@extends('adminlte::page')

@section('title', 'TION')

@section('content_header')
    <h1 class="m-0 text-dark">Meditations list</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title"></h3>
                    <div class="card-tools">
{{--                        <div class="input-group input-group-sm" style="width: 150px;">--}}
{{--                            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">--}}

{{--                            <div class="input-group-append">--}}
{{--                                <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover text-nowrap">
                        <thead>
                        <tr>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Meditation</th>
                            <th>Created At</th>
                            <th>Owner</th>
                            <th>Blesses</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($meditations as $meditation)
                        <tr>
                            <td style="width: 100px;">
                                <div style="height: 100px; overflow: hidden;">
                                    @if($meditation->image_path)
                                        <img src="{{$meditation->image_path}}" style="width: 100px; background: grey;">
                                    @else
                                        <div style="background: grey; width: 100px; height: 100%"></div>
                                    @endif
                                </div>
                            </td>
                            <td>{{$meditation->title}}</td>
                            <td>{{$meditation->created_at->format("Y-m-d H:i")}}</td>
                            <td>
                                <audio controls style="width: 250px">
                                    <source src="{{$meditation->file_path}}" type="audio/mpeg">
                                    Your browser does not support the audio element.
                                </audio></td>
                            <td>
                                <a href="{{route("user-details",$meditation->user->uuid)}}">{{$meditation->user->name}}</a><br/>
                                <a href="mailto:{{$meditation->user->email}}">{{$meditation->user->email}}</a>
                            </td>
                            <td>{{$meditation->tot_bless}}</td>
                            <td><a href="{{route("user-details", $meditation->id)}}" class="btn btn-success btn-block"><b>Details</b></a></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer clearfix">
                    {{ $meditations->links() }}
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@stop

@section('css')

@stop

@section('js')

@stop


