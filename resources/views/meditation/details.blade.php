@extends('adminlte::page')

@section('title', 'TION')

@section('content_header')
    <h1 class="m-0 text-dark">Profile</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-3">
            <!-- Profile Image -->
            <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                    <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle" src="{{$user->image_path}}" alt="User profile picture">
                    </div>

                    <h3 class="profile-username text-center">{{$user->name}}</h3>

                    <p class="text-muted text-center"><a href="mailto:{{$user->email}}">{{$user->email}}</a></p>

                    <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                            <b>Followers</b> <a class="float-right">{{$userInfo["followers"]}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Following</b> <a class="float-right">{{$userInfo["following"]}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Meditations</b> <a class="float-right">{{count($userInfo["meditations"])}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Courses</b> <a class="float-right">{{$userInfo["courses"]}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Groups</b> <a class="float-right">{{$userInfo["groups"]}}</a>
                        </li>
                    </ul>
                    @if ($user->is_admin)
                        <a href="{{route("user.remove-from-admin", $user->uuid)}}" class="btn btn-danger btn-block"><b>Remove from admin</b></a>
                    @else
                        <a href="{{route("user.set-as-admin", $user->uuid)}}" class="btn btn-success btn-block"><b>Set as admin</b></a>
                    @endif

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Achievements</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <ul class="users-list clearfix">
                        @foreach($userInfo["achievements"] as $achievement)
                            <li>
                                <img src="{{$achievement->image_path}}" alt="User Image" width="100">
                            </li>
                        @endforeach
                    </ul>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="card">
                <div class="card-header p-2">
                    <ul class="nav nav-pills">
                        <li class="nav-item"><a class="nav-link active" href="#timeline" data-toggle="tab">Timeline</a></li>
                        <li class="nav-item"><a class="nav-link" href="#meditation" data-toggle="tab">Meditations</a></li>
                        <li class="nav-item"><a class="nav-link" href="#courses" data-toggle="tab">Courses</a></li>
                    </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="timeline">
                            @foreach($userInfo["activities"] as $timeline)
                            <!-- Post -->
                            <div class="post">
                                <p>{{$timeline->desc}}</p>
                                @if($timeline->image_path)
                                    <div class="row mb-3">
                                        <div class="col-sm-6">
                                            <img class="img-fluid" src="{{$timeline->image_path}}" alt="Photo">
                                        </div>
                                    </div>
                                @endif
                                <p>
                                    <label><i class="far fa-comments mr-1"></i> Comments: </label> {{$timeline->tot_comment}}<br>
                                    <label><i class="far fa-thumbs-up mr-1"></i> Bless:</label> {{$timeline->tot_bless}}
                                </p>
                                <span class="description">Sent - {{$timeline->created_at->diffForHumans()}}</span>
                            </div>
                            <!-- /.post -->
                            @endforeach
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="meditation">
                            <div class="row">
                                @foreach($userInfo["meditations"] as $meditation)
                                    <div class="col-md-3">
                                        <div class="card">
                                            <div class="card-header" style="height: 180px; overflow: hidden;">
                                                <img src="{{$meditation->thumb_path}}" style="width: 100%">
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <p>{{$meditation->title}}</p>
                                                <p><label>Blesses:</label> {{$meditation->tot_bless}}</p>
                                                <small>{{$meditation->created_at->format('Y-m-d H:i')}}</small>
                                            </div>
                                            <!-- /.card-body -->
                                            <div class="card-footer">
                                                <audio controls style="width: 100%">
                                                    <source src="{{$meditation->file_path}}" type="audio/mpeg">
                                                    Your browser does not support the audio element.
                                                </audio>
                                            </div>
                                            <!-- /.card-footer -->
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <!-- /.tab-pane -->

                        <div class="tab-pane" id="courses">
                            <div class="row">
                                @foreach($user->courses as $course)
                                    <div class="col-md-3">
                                        <div class="card">
                                            <div class="card-header" style="height: 180px; overflow: hidden;">
                                                @if($course->image_path)
                                                    <img src="{{$course->image_path}}" style="width: 100%; background: grey;">
                                                @else
                                                    <div style="background: grey; width: 100%; height: 100%"></div>
                                                @endif
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <p>{{$course->name}}</p>
                                                    <p><label>Prices:</label> R$:
                                                        @if($course->price)
                                                            {{$course->price->price/100}}
                                                        @else
                                                            0.00
                                                        @endif
                                                    </p>

                                                @if($course->signatures)
                                                    <p><label>Signatures:</label> {{$course->signatures()->count()}}</p>
                                                @endif

                                                <small>{{$course->created_at->format('Y-m-d H:i')}}</small>
                                            </div>
                                            <!-- /.card-footer -->
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
@stop

@section('css')
    <style>
        .meditation-img {
            display: block;
            max-width: 100%;
            max-height: 100%;
            width: auto;
            height: auto;
        }
    </style>

@stop

@section('js')

@stop


