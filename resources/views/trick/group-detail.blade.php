@extends('adminlte::page')

@section('title', 'Pataki')

@section('content_header')
    <h1 class="m-0 text-dark"><a href="{{route("trick.index")}}">Truques</a></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-3">
            <!-- Profile Image -->
            <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                    <h3 class="profile-username text-center">{{$group->name}}</h3>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="card card-dark">
                <div class="card-header p-2">

                    <h3 class="card-title">Truques</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" onclick="$('#mdlTrick').modal()">
                            <i class="fas fa-plus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tricks">
                                <div class="row">
                                    @foreach($group->tricks as $trick)
                                        <div class="col-md-3">
                                            <div class="card">
                                                <div class="card-header" style="height: 180px; overflow: hidden;">
                                                    @if($trick->image_path)
                                                        <img src="{{$trick->image_path}}" style="width: 100%; background: grey;">
                                                    @else
                                                        <div style="background: grey; width: 100%; height: 100%"></div>
                                                    @endif
                                                </div>
                                                <!-- /.card-header -->
                                                <div class="card-body">
                                                    <p>{{$trick->title}}</p>
                                                </div>
                                                <div class="card-footer">
                                                    <a href="{{route("trick.detail", [$group->id, $trick->id])}}" class="btn btn-success btn-block"><b>Detalhes</b></a>
                                                </div>
                                                <!-- /.card-footer -->
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                        </div>
                        <!-- /.tab-pane -->
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
    <div class="modal fade " id="mdlTrick" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdlStepTitle">Novo truque</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="mt-3" action="{{route("trick.trick-action")}}" id="formSteps"  method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="group_id" value="{{$group->id}}">
                <div class="modal-body pt-0">
                    <div class="form-group">
                        <label for="txtTitle">Título</label>
                        <input type="text" name="title" class="form-control" id="txtTitle" value="">
                    </div>
                    <div class="form-group">
                        <label for="txtDesc">Descrição</label>
                        <textarea id="txtDesc" class="form-control" name="desc"></textarea>

                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Dificuldade</label>
                        <select class="form-control" name="difficulty">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="4">5</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="txtImage">Imagem</label>
                        <div class="input-group">
                            <input type="file" name="image" id="txtImage">
                        </div>
                    </div>
                </div>
                </form>

                <div class="modal-footer" style="text-align: right;">
                    <button  type="button" data-dismiss="modal" aria-label="Close"  class="mb-2 mr-2 btn btn btn-outline-danger">
                        <i class="fa fa-times btn-icon-wrapper"></i> Cancelar
                    </button>
                    <button class="mb-2 mr-2 btn btn btn-success" onclick="$('#formSteps').submit();">
                        <i class="fa fa-save btn-icon-wrapper"></i> Salvar
                    </button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <style>
        .meditation-img {
            display: block;
            max-width: 100%;
            max-height: 100%;
            width: auto;
            height: auto;
        }
    </style>

@stop

@section('js')

@stop


