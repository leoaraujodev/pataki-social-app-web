@extends('adminlte::page')

@section('title', 'Pataki')

@section('content_header')
    <h1 class="m-0 text-dark">
        <a href="{{route("trick.index")}}">Truques</a></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-3">
            <!-- Profile Image -->
            <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                    <h3 class="profile-username text-center"><a href="{{route("trick.group-detail", $trick->group->id)}}">{{$trick->group->name}}</a></h3>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tricks">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">Truque</h3>
                                </div>
                                @if($trick->file)
                                    <div class="row">
                                        <div class="col-6 text-center">
                                            <img class="mt-2" style="width: 50%" src="{{$trick->image_path}}">
                                        </div>
                                        <div class="col-6">
                                @endif
                                        <form role="form" action="{{route("trick.update",$trick->id )}}" method="POST" enctype="multipart/form-data">
                                    <input type="hidden" name="_method" value="PUT">
                                    {!! csrf_field() !!}
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="txtTitle">Título</label>
                                            <input type="text" name="title" class="form-control" id="txtTitle" value="{{$trick->title}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="txtDesc">Descrição</label>
                                            <textarea id="txtDesc" class="form-control" name="desc">{{$trick->desc}}</textarea>

                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Dificuldade</label>
                                            <select class="form-control" name="difficulty">
                                                <option value="1" @if($trick->difficulty == 1) selected @endif>1</option>
                                                <option value="2" @if($trick->difficulty == 2) selected @endif>2</option>
                                                <option value="3" @if($trick->difficulty == 3) selected @endif>3</option>
                                                <option value="4" @if($trick->difficulty == 4) selected @endif>4</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtImage">Imagem</label>
                                            <div class="input-group">
                                                <input type="file" name="image" id="txtImage">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->

                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary">Salvar</button>
                                    </div>
                                </form>
                                @if($trick->file)
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="tab-pane active" id="tricks">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">Passos</h3>
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" onclick="openModalNewStep();" data-card-widget="collapse">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Passo</th>
                                        <th>Ordem</th>
                                        <th>Ações</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($trick->steps as $step)
                                        <tr>
                                            <td><img class="mt-2" style="width: 50px" src="{{$step->image_path}}"></td>
                                            <td id="stepDesc_{{$step->id}}">{{$step->desc}}</td>
                                            <td id="stepOrder_{{$step->id}}">{{$step->order}}</td>
                                            <td>
                                                <a href="javascript:editStep({{$step->id}})" class="btn btn-success btn-block"><b>Editar</b></a>
                                                <a href="javascript:deleteStep({{$step->id}})" class="btn btn-danger btn-block"><b>Deletar</b></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                        <div class="tab-pane active" id="tricks">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">Dicas</h3>
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" onclick="openModalNewTip();" data-card-widget="collapse">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Dica</th>
                                        <th>Ordem</th>
                                        <th>Ações</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($trick->tips as $tip)
                                        <tr>
                                            <td id="tipTip_{{$tip->id}}">{{$tip->tip}}</td>
                                            <td id="tipOrder_{{$tip->id}}">{{$tip->order}}</td>
                                            <td>
                                                <a href="javascript:editTip({{$tip->id}})" class="btn btn-success btn-block"><b>Editar</b></a>
                                                <a href="javascript:deleteTip({{$tip->id}})" class="btn btn-danger btn-block"><b>Deletar</b></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>


    <div class="modal fade " id="mdlStep" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdlStepTitle"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pt-0">
                    <form class="mt-3" action="{{route("trick.step-action")}}" id="formSteps"  method="POST" enctype="multipart/form-data">
                        <input type="hidden" value="{{$trick->id}}" name="trick_id" id="mdlStrickId">
                        <input type="hidden" value="" name="step_id" id="mdlStepId">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="mdlStepStep">Passo</label>
                            <textarea id="mdlStepStep" class="form-control" name="desc"></textarea>

                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Ordem</label>
                            <input type="number" id="mdlStepOrder" class="form-control" name="order">
                        </div>
                        <div class="form-group">
                            <label for="txtImage">Imagem</label>
                            <div class="input-group">
                                <input type="file" name="image" id="txtImage">
                            </div>
                        </div>
                    </form>

                </div>
                <div class="modal-footer" style="text-align: right;">
                    <button  type="button" data-dismiss="modal" aria-label="Close"  class="mb-2 mr-2 btn btn btn-outline-danger">
                        <i class="fa fa-times btn-icon-wrapper"></i> Cancelar
                    </button>
                    <button class="mb-2 mr-2 btn btn btn-success" onclick="$('#formSteps').submit();">
                        <i class="fa fa-save btn-icon-wrapper"></i> Salvar
                    </button>
                </div>
            </div>
        </div>
    </div>
    <form action="{{route("trick.step-delete")}}" id="formStepDelete"  method="POST" >
        {{ csrf_field() }}
        <input type="hidden" value="" name="step_id" id="mdlDeleteStepId">
    </form>

    <div class="modal fade " id="mdlTip" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdlTipTitle"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pt-0">
                    <form class="mt-3" action="{{route("trick.tip-action")}}" id="formTip"  method="POST" enctype="multipart/form-data">
                        <input type="hidden" value="{{$trick->id}}" name="trick_id" id="mdlTipStrickId">
                        <input type="hidden" value="" name="tip_id" id="mdlTipId">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="mdlTipTip">Dica</label>
                            <textarea id="mdlTipTip" class="form-control" name="tip"></textarea>

                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Ordem</label>
                            <input type="number" id="mdlTipOrder" class="form-control" name="order">
                        </div>
                    </form>

                </div>
                <div class="modal-footer" style="text-align: right;">
                    <button  type="button" data-dismiss="modal" aria-label="Close"  class="mb-2 mr-2 btn btn btn-outline-danger">
                        <i class="fa fa-times btn-icon-wrapper"></i> Cancelar
                    </button>
                    <button class="mb-2 mr-2 btn btn btn-success" onclick="$('#formTip').submit();">
                        <i class="fa fa-save btn-icon-wrapper"></i> Salvar
                    </button>
                </div>
            </div>
        </div>
    </div>
    <form action="{{route("trick.tip-delete")}}" id="formTipDelete"  method="POST" >
        {{ csrf_field() }}
        <input type="hidden" value="" name="tip_id" id="mdlDeleteTipId">
    </form>
@stop

@section('css')
    <style>
        .meditation-img {
            display: block;
            max-width: 100%;
            max-height: 100%;
            width: auto;
            height: auto;
        }
    </style>

@stop

@section('js')
<script>
    function deleteTip(stepId) {
        if(confirm("Deseja realmente deletar?")) {
            $("#mdlDeleteTipId").val(stepId);
            $('#formTipDelete').submit();
        }
    }

    function editTip(tipId)
    {
        $('#formTip')[0].reset();
        $("#mdlTipTitle").html("Editar dica");
        $("#mdlTipId").val(tipId);
        $("#mdlTipStrickId").val({{$trick->id}});

        $("#mdlTipTip").val($("#tipTip_"+tipId).html());
        $("#mdlTipOrder").val($("#tipOrder_"+tipId).html());
        $('#mdlTip').modal();
    }

    function openModalNewTip()
    {
        $("#mdlTipTitle").html("Nova dica");
        $("#mdlTipId").val("");
        $('#formTip')[0].reset();
        $("#mdlTipStrickId").val({{$trick->id}});
        $('#mdlTip').modal();
    }

    function deleteStep(stepId) {
        if(confirm("Deseja realmente deletar?")) {
            $("#mdlDeleteStepId").val(stepId);
            $('#formStepDelete').submit();
        }
    }

    function editStep(stepId)
    {
        $('#formSteps')[0].reset();
        $("#mdlStepTitle").html("Editar passo");
        $("#mdlStepId").val(stepId);
        $("#mdlStrickId").val({{$trick->id}});

        $("#mdlStepStep").val($("#stepDesc_"+stepId).html());
        $("#mdlStepOrder").val($("#stepOrder_"+stepId).html());
        $('#mdlStep').modal();
    }

    function openModalNewStep()
    {
        $("#mdlStepTitle").html("Novo passo");
        $("#mdlStepId").val("");
        $('#formSteps')[0].reset();
        $("#mdlStrickId").val({{$trick->id}});
        $('#mdlStep').modal();
    }

</script>
@stop


