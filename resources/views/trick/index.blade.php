@extends('adminlte::page')

@section('title', 'Pataki')

@section('content_header')
    <h1 class="m-0 text-dark">Grupos de truques</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title"></h3>
                    <div class="card-tools">
{{--                        <div class="input-group input-group-sm" style="width: 150px;">--}}
{{--                            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">--}}

{{--                            <div class="input-group-append">--}}
{{--                                <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Grupos</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($groups as $group)
                        <tr>
                            <td>{{$group->name}}</td>
                            <td class="w-25"><a href="{{route("trick.group-detail", $group->id)}}" class="btn btn-success btn-block"><b>Detalhes</b></a></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer clearfix">
                    {{ $groups->links() }}
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@stop

@section('css')

@stop

@section('js')

@stop


