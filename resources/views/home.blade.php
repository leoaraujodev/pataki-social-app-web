@extends('adminlte::page')

@section('title', 'Pataki')

@section('content_header')
    <h1 class="m-0 text-dark">Dashboard</h1>
@stop

@section('content')
    <div class="row">
        <section class="col-lg-12 connectedSortable">
            <div class="row">
            <!-- ./col -->
            <div class="col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                    <div class="inner">
                        <h3>{{$countNewUsers}}</h3>
                        <p>New users <small>(Last 30 days)</small></p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
{{--                    <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>--}}
                </div>
            </div>


            <div class="col-6">
                <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{$countNewPosts}}</h3>

                        <p>New posts <small>(Last 30 days)</small></p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-fw  fa-facebook-messenger"></i>
                    </div>
{{--                    <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>--}}
                </div>
            </div>
            <!-- ./col -->
        </div>
{{--            <div class="row">--}}
{{--                <div class="col-lg-6 col-6">--}}
{{--                    <!-- USERS LIST -->--}}
{{--                    <div class="card">--}}
{{--                        <div class="card-header">--}}
{{--                            <h3 class="card-title">Main teachers</h3>--}}

{{--                            <div class="card-tools">--}}
{{--                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <!-- /.card-header -->--}}
{{--                        <div class="card-body p-0">--}}
{{--                            <ul class="users-list clearfix">--}}
{{--                                @foreach($mainTeachers as $meditation)--}}
{{--                                    @if($meditation['user'])--}}
{{--                                        <li>--}}
{{--                                            <img src="{{$meditation['user']['image_path']}}" alt="User Image" width="100">--}}
{{--                                            <a class="users-list-name" href="{{route("user-details",$meditation['user']['uuid'])}}">{{$meditation['user']['name']}}</a>--}}
{{--                                        </li>--}}
{{--                                    @endif--}}
{{--                                @endforeach--}}
{{--                            </ul>--}}
{{--                            <!-- /.users-list -->--}}
{{--                        </div>--}}
{{--                        <!-- /.card-footer -->--}}
{{--                    </div>--}}
{{--                    <!--/.card -->--}}
{{--                </div>--}}
{{--                <div class="col-lg-6 col-6">--}}
{{--                    <div class="card card-outline">--}}
{{--                    <div class="card-header">--}}
{{--                        <h3 class="card-title">--}}
{{--                            <i class="far fa-chart-bar"></i>--}}
{{--                            Top 5 hashtags used <small>(Last 30 days)</small>--}}
{{--                        </h3>--}}

{{--                        <div class="card-tools">--}}
{{--                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>--}}
{{--                            </button>--}}
{{--                            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>--}}
{{--                            </button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="card-body">--}}
{{--                        <div id="donut-chart" style="height: 300px;"></div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                </div>--}}
{{--            </div>--}}
        </section>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
@stop

@section('js')
    <script src="https://adminlte.io/themes/dev/AdminLTE/plugins/flot/jquery.flot.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flot/0.8.3/jquery.flot.pie.min.js"></script>

    <script>
        $(function () {
            /*
             * DONUT CHART
             * -----------
             */

            var donutData = [
{{--                @foreach($countTags as $countTag)--}}
{{--                {--}}
{{--                    label: '{{$countTag['tag']->tag}}',--}}
{{--                    data : {{$countTag['count']}},--}}
{{--                    // color: '#3c8dbc'--}}
{{--                },--}}
{{--                @endforeach--}}
            ]
            $.plot('#donut-chart', donutData, {
                series: {
                    pie: {
                        show       : true,
                        radius     : 1,
                        innerRadius: 0.2,
                        label      : {
                            show     : true,
                            radius   : 2 / 3,
                            formatter: labelFormatter,
                            threshold: 0.1
                        }

                    }
                },
                legend: {
                    show: false
                }
            })
            /*
             * END DONUT CHART
             */

        })

        /*
         * Custom Label formatter
         * ----------------------
         */
        function labelFormatter(label, series) {
            return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
                + label
                + '<br>'
                + Math.round(series.percent) + '%</div>'
        }
    </script>
@stop


