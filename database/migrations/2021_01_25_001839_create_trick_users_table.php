<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrickUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trick_users', function (Blueprint $table) {
            $table->id();
            $table->enum('state', ['completed', 'in_progress','no_progress'])->default('no_progress');
            $table->integer('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('trick_id');
            $table->foreign('trick_id')->references('id')->on('tricks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trick_users');
    }
}
