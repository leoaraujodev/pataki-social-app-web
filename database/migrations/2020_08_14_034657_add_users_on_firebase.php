<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\User;
use App\Firestore;

class AddUsersOnFirebase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (User::get() as $user) {
            $data = ["fields"=>["id"=>["integerValue"=> $user->id]]];
            $result = Firestore::postMessage("users_123?documentId=".$user->uuid, $data);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
