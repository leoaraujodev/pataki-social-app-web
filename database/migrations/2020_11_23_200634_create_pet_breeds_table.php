<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePetBreedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pet_breeds', function (Blueprint $table) {
            $table->id();
            $table->integer('type_id');
            $table->foreign('type_id')->references('id')->on('pet_types');
            $table->string("name");
            $table->timestamps();
        });

        $dogs = [
            "	Afghan Hound	",
            "	Airedale terrier	",
            "	Akita Americano	",
            "	Akita Inu	",
            "	American Pitbull	",
            "	American Pitbull Terrier	",
            "	Basset Hound	",
            "	Beagle	",
            "	Bichon Frisé	",
            "	Boxer	",
            "	Bull Terrier	",
            "	Bulldog	",
            "	Bullmastiff	",
            "	Chihuahua	",
            "	Chow Chow	",
            "	Cocker Americano	",
            "	Cocker Inglês	",
            "	Cocker Spaniel Inglês	",
            "	Collie	",
            "	Dálmata	",
            "	Dobermann	",
            "	Dogue Alemão	",
            "	Fila Brasileiro	",
            "	Fox Terrier	",
            "	Foxhound Inglês	",
            "	Golden Retriever	",
            "	Husky Siberiano	",
            "	Labrador	",
            "	Lhasa Apso	",
            "	Lulu da Pomerânia	",
            "	Maltês	",
            "	Mastiff	",
            "	Mastino Napoletano	",
            "	O. E. Sheepdog	",
            "	Old English Sheepdog	",
            "	Pastor Alemão	",
            "	Pequinês	",
            "	Perdigueiro	",
            "	Perdigueiro Português	",
            "	Pinscher	",
            "	Pointer Inglês	",
            "	Poodle	",
            "	Retriever do Labrador	",
            "	Rottweiler	",
            "	São Bernardo	",
            "	Schnauzer	",
            "	Setter Inglês	",
            "	Setter Irlandês	",
            "	Shar Pei	",
            "	Shih Tzu	",
            "	Spitz Alemão	",
            "	Staff Bull Terrier	",
            "	Staffordshire Bull Terrier	",
            "	Teckel	",
            "	Terrier Brasileiro	",
            "	Yorkshire Terrier	"];

            foreach ($dogs as $dog) {
                \App\PetBreed::create(["type_id"=> 1, "name" => trim($dog)]);
            }

            $cats = ["	Abyssinian	",
                "	American Bobtail Longhair	",
                "	American Bobtail Shorthair	",
                "	American Shorthair	",
                "	American Wirehair	",
                "	Arabian Mau	",
                "	Asian Semi-long Hair	",
                "	Australian Mist	",
                "	Bengal	",
                "	Bobtail Japonês	",
                "	Bombaim	",
                "	Brazilian Shorthair	",
                "	British Longhair	",
                "	Burmês	",
                "	California Spangled Cat	",
                "	Chausie	",
                "	Cornish Rex	",
                "	Curl Americano Pelo Curto	",
                "	Curl Americano Pelo Longo	",
                "	Cymric	",
                "	Devon Rex	",
                "	Domestic Long-Haired	",
                "	Domestic Short-Haired	",
                "	Don Sphynx	",
                "	Egyptian Mau	",
                "	European	",
                "	Exotic Shorthair	",
                "	German Rex	",
                "	Havana	",
                "	Himalaio	",
                "	Khao Manee	",
                "	Korat	",
                "	Kurilian Bobtail Longhair	",
                "	Kurilian Bobtail Shorthair	",
                "	LaPerm Longhair	",
                "	LaPerm Shorthair	",
                "	Maine Coon	",
                "	Manx	",
                "	Mekong Bobtail	",
                "	Munchkin Longhair	",
                "	Munchkin Shorthair	",
                "	Nebelung	",
                "	Norwegian Forest Cat	",
                "	Ocicat	",
                "	Ojos Azules Shorthair	",
                "	Oriental Longhair	",
                "	Oriental Shorthair	",
                "	Persa	",
                "	Peterbald	",
                "	Pixiebob Longhair	",
                "	Pixiebob Shorthair	",
                "	Ragamuffin	",
                "	Ragdoll	",
                "	Russo Azul	",
                "	Sagrado da Birmânia	",
                "	Savannah Cat	",
                "	Scottish Fold	",
                "	Selkirk Rex Longhair	",
                "	Selkirk Rex Shorthair	",
                "	Serengeti	",
                "	Siamês	",
                "	Siberian	",
                "	Singapura	",
                "	Snowshoe	",
                "	Sokoke	",
                "	Somali	",
                "	Sphynx	",
                "	Thai	",
                "	Tonkinese Shorthair	",
                "	Toyger	",
                "	Turkish Angorá	",
                "	Turkish Van	",
                "	York Chocolate	"];

        foreach ($cats as $cat) {
            \App\PetBreed::create(["type_id"=> 2, "name" => trim($cat)]);
        }
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pet_breeds');
    }
}
