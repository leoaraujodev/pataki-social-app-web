<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChatMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_messages', function (Blueprint $table) {
            $table->id();
            $table->integer('room_id');
            $table->foreign('room_id')->references('id')->on('chat_rooms');
            $table->integer('sender_id');
            $table->foreign('sender_id')->references('id')->on('users');
            $table->text('message')->nullable();
            $table->string('file')->nullable();
            $table->integer('sticker_id')->nullable();
            $table->foreign('sticker_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_messages');
    }
}
