<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePetNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pet_notifications', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->integer('pet_id');
            $table->foreign('pet_id')->references('id')->on('pets');
            $table->string('time')->nullable();
            $table->boolean('on_sun')->default(false);
            $table->boolean('on_mon')->default(false);
            $table->boolean('on_tue')->default(false);
            $table->boolean('on_wed')->default(false);
            $table->boolean('on_thu')->default(false);
            $table->boolean('on_fri')->default(false);
            $table->boolean('on_sat')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pet_notifications');
    }
}
