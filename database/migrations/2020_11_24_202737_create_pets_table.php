<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pets', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string("filename")->nullable();
            $table->enum('size', ['S', 'M','L']);
            $table->double("weight");
            $table->integer('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('breed_id');
            $table->foreign('breed_id')->references('id')->on('pet_breeds');
            $table->date('birth_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pets');
    }
}
