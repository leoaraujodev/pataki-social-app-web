<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTricksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tricks', function (Blueprint $table) {
            $table->id();
            $table->integer('group_id');
            $table->foreign('group_id')->references('id')->on('tricks_groups');
            $table->string("title");
            $table->text("desc");
            $table->integer("difficulty")->default(0);
            $table->string("file")->nullable();
            $table->integer("order")->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tricks');
    }
}
