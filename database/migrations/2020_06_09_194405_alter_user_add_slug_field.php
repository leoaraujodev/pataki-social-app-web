<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;

class AlterUserAddSlugField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
        Schema::table('users', function ($table) {
            $table->uuid('uuid')->nullable();
        });
        DB::statement('ALTER TABLE users ALTER COLUMN uuid SET DEFAULT uuid_generate_v4();');
        foreach (\App\User::all() as $user) {
            $user->uuid = Uuid::generate(4);
            $user->update();
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP EXTENSION IF EXISTS "uuid-ossp";');
        Schema::table('users', function ($table) {
            $table->dropColumn('uuid');
        });
    }
}
