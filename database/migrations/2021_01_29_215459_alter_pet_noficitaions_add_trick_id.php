<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPetNoficitaionsAddTrickId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pet_notifications', function (Blueprint $table)
        {

            $table->integer("pet_id")->nullable()->change();

            $table->integer('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('trick_id')->nullable();
            $table->foreign('trick_id')->references('id')->on('tricks');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pet_notifications', function ($table) {
            $table->dropColumn('trick_id');
        });
    }
}
