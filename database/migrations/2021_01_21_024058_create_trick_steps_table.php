<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrickStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trick_steps', function (Blueprint $table) {
            $table->id();
            $table->integer('trick_id');
            $table->foreign('trick_id')->references('id')->on('tricks');
            $table->text("desc");
            $table->string("file")->nullable();
            $table->integer("order")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trick_steps');
    }
}
