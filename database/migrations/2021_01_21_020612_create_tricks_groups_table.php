<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\TricksGroups;

class CreateTricksGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tricks_groups', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->text("desc");
            $table->integer("order")->nullable();
            $table->timestamps();
        });

        TricksGroups::create([  "name" => "Básico",
                                "desc" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                                "order"=> 0]);

        TricksGroups::create([  "name" => "Intermediário",
                                "desc" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                                "order"=> 1]);

        TricksGroups::create([  "name" => "Avançado",
                                "desc" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                                "order"=> 2]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tricks_groups');
    }
}
