<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\User;
use App\Pet;

class AddUserDefaultPublish extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $user = User::find(10);
        $user->name = "Pataki";
        $user->email = "contato@pataki.com.br";
        $user->update();

        Pet::insert([   "id" => 10,
                        "name" => "Pataki",
                        "size" => "S",
                        "weight" => 1,
                        'breed_id' => \App\PetBreed::where("name",'=','SRD')->first()->id,
                        "user_id" => 10]);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
