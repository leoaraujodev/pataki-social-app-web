<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ChatSticker;

class CreateChatStickersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_stickers', function (Blueprint $table) {
            $table->id();
            $table->string('file');
            $table->timestamps();
        });

        ChatSticker::create(["file"=>"stickers/s1.png"]);
        ChatSticker::create(["file"=>"stickers/s2.png"]);
        ChatSticker::create(["file"=>"stickers/s3.png"]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_stickers');
    }
}
