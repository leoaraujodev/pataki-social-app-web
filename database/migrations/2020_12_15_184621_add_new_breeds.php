<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewBreeds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pet_breeds', function ($table) {
            $table->integer('sort')->nullable();
        });

        $dogs = [[  "name"=> "SRD",
                        "sort" => 1],
                    ["name" => "Border Collie",
                        "sort" => null]];

        foreach ($dogs as $dog) {

            \App\PetBreed::create(["type_id"=> 1, "name" => trim($dog["name"]), "sort" => $dog["sort"]]);
        }

        $cats = [
            [  "name"=> "SRD",
                "sort" => 1]];

        foreach ($cats as $cat) {
            \App\PetBreed::create(["type_id"=> 2, "name" => trim($cat["name"]), "sort" => 1]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pet_breeds', function ($table) {
            $table->dropColumn('sort');
        });
    }
}
