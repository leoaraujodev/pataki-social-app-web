<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_notifications', function (Blueprint $table) {
            $table->id();
            $table->enum('action', ['follow', 'post_comment','post_like'])->default('follow');
            $table->integer('notified_id')->nullable();
            $table->foreign('notified_id')->references('id')->on('users');
            $table->integer('pet_id')->nullable();
            $table->foreign('pet_id')->references('id')->on('pets');
            $table->integer('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('timeline_id')->nullable();
            $table->foreign('timeline_id')->references('id')->on('timelines');
            $table->string("message")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_notifications');
    }
}
