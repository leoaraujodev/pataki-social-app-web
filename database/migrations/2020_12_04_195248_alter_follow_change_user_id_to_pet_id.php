<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\UserFollow;

class AlterFollowChangeUserIdToPetId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        UserFollow::truncate();
        Schema::table('user_follows', function (Blueprint $table)
        {
            $table->dropColumn('followed_id');
            $table->integer('pet_id');
            $table->foreign('pet_id')->references('id')->on('pets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_follows', function ($table) {
            $table->dropColumn('pet_id');
        });
    }
}
