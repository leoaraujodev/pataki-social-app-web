<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTimelineAddField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('timelines', function ($table) {
            $table->enum('type', ['single', 'compound'])->default('single');
            $table->text('desc')->nullable()->change();
            $table->string("header_text")->nullable();
            $table->string("text")->nullable();
            $table->integer('meditation_id')->nullable();
            $table->foreign('meditation_id')->references('id')->on('meditations');
            $table->integer('course_id')->nullable();
            $table->foreign('course_id')->references('id')->on('courses');
            $table->integer('comp_group_id')->nullable();
            $table->foreign('comp_group_id')->references('id')->on('groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('timelines', function ($table) {
            $table->dropColumn('type');
            $table->dropColumn('header_text');
            $table->dropColumn('text');
            $table->dropColumn('meditation_id');
            $table->dropColumn('course_id');
            $table->dropColumn('comp_group_id');
        });
    }
}
